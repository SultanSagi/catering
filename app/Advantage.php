<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advantage extends BaseModel
{
    public function getWebpImageAttribute()
    {
        return str_replace('.' . pathinfo(\Voyager::image($this->image),PATHINFO_EXTENSION), '.webp', \Voyager::image($this->image));
    }

    public function getBigThumbAttribute()
    {
        return  \Voyager::image($this->getThumbnail($this->image,'big'));
    }
}
