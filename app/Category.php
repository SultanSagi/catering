<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Category extends \TCG\Voyager\Models\Category
{
    use Resizable;
    /**
     * Get the route key for the model.
     *
     * @return string
     */

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected $dates = ['created_at','updated_at'];


    public function products()
    {
        return $this->hasMany(Product::class)->where('active',true)->orderBy('sort_id');
    }

    public function categories()
    {
        return $this->hasMany(self::class, 'parent_id')->where('active',true)
            ->orderBy('order', 'ASC');
    }

    public function parent()
    {
        return $this->parentId();
    }

    public function getParentsAttribute()
    {
        $parents = collect([]);

        $parent = $this->parent;

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents;
    }


    public function getLinkAttribute()
    {
        $categories = $this->parents->reverse();
        $categories->push($this);
        return '/catalog/' . $categories->pluck('slug')->implode('/');

    }

    public function parentId()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setThumbnailAttribute($value)
    {
        if ($value) {
            $this->attributes['thumb'] = str_replace('.' . pathinfo(\Voyager::image($value),PATHINFO_EXTENSION), '-small.webp', \Voyager::image($value));
        }else {
            $this->attributes['thumb'] = '/images/nophoto.png';
        }
    }

    public static function scopeSearch($query, $searchTerm)
    {
        return $query->where('name', 'like', '%' .$searchTerm. '%')
            ->orWhere('slug', 'like', '%' .$searchTerm. '%');
    }


    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setThumbnailSmallAttribute($value)
    {
        if ($value){
            $this->attributes['thumbnailSmall'] = str_replace('.'.pathinfo(\Voyager::image($value),PATHINFO_EXTENSION),'-small.webp',\Voyager::image($value));
        }else {
            $this->attributes['thumbnailSmall'] = '/images/nophoto.png';
        }
    }
    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setWebpAttribute($value)
    {
        if ($value) {
            $this->attributes['webp'] = str_replace('.' . pathinfo(\Voyager::image($value),PATHINFO_EXTENSION), '.webp', \Voyager::image($value));;
        } else {
            $this->attributes['webp'] = '/images/nophoto.png';
        }
    }

    public function getWebpImageAttribute()
    {
        return str_replace('.' . pathinfo(\Voyager::image($this->image),PATHINFO_EXTENSION), '.webp', \Voyager::image($this->image));
    }


    public function getBigThumbAttribute()
    {
        return \Voyager::image($this->getThumbnail($this->image,'big'));
    }

    public function getThumbicAttribute()
    {
        return $this->thumbnail('small','image');
    }
}
