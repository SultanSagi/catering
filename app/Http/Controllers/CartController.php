<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderEntity;
use App\OrderProduct;
use App\Page;
use App\Payment;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Paybox\Pay\Facade as Paybox;

class CartController extends Controller
{
    public function addToCart(Request $request,$productId)
    {
        $product = Product::with('category')->whereId($productId)->first();
        if ($product->active) {
            \Cart::add(['id' => $product->id, 'name' => $product->name, 'quantity' => intval($request->get('qty')), 'price' => $product->price,
                'attributes' => [
                    'thumbnail' => $product->thumbic,
                    'name' => $product->name,
                    'description' => $product->description,
                    'price' => $product->price,
                    'price_value' => $product->price_value,
                    'min_value' => $product->min_value,
                    'category' => $product->category->name,
                    'size' => $product->size,
                    'weight' => $product->weight,
                    'tags' => $product->tags,
                ]]);
            $cart = \Cart::getContent();
            return response()->json(['cart' => $cart]);
        }
        else {
            $cart = \Cart::getContent();
            return response()->json(['cart'=>$cart]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCart(Request $request)
    {
        if ($request->get('itemQty') == 0) {
            \Cart::remove($request->get('itemId'));
        } else {
            \Cart::update($request->get('itemId'), ['quantity' => ['relative' => false, 'value' => $request->get('itemQty')]]);
        }

        $products = \Cart::getContent();
        $subtotal = \Cart::getTotal();
        $itemsCount = \Cart::getTotalQuantity();
        foreach ($products as $product) {
            $product->attributes['image_link'] = \Voyager::image($product->attributes['thumbnail']);
        }
        return response()->json(['products' => $products, 'subtotal' => $subtotal, 'itemsCount' => $itemsCount]);
    }

    public function removeFromCart(Request $request)
    {
        \Cart::remove($request->get('itemId'));

        $products = \Cart::getContent();
        $subtotal = \Cart::getTotal();
        $itemsCount = \Cart::getTotalQuantity();
        foreach ($products as $product) {
            $product->attributes['image_link'] = \Voyager::image($product->attributes['thumbnail']);
        }
        return response()->json(['products' => $products, 'subtotal' => $subtotal, 'itemsCount' => $itemsCount]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $cart = \Cart::getContent();
        $subtotal = \Cart::getSubTotal();
        $cartItems = $cart->count();
        $page = Page::where('type','cart')->where('status',Page::STATUS_ACTIVE)->first();
        return view('cart.cart', compact('cart', 'subtotal', 'cartItems', 'page'));
    }

    public function getCartContent()
    {
        $products = \Cart::getContent();
        $subtotal = \Cart::getTotal();
        $itemsCount = \Cart::getTotalQuantity();
        foreach ($products as $product) {
            $product->attributes['image_link'] = \Voyager::image($product->attributes['thumbnail']);
        }
        return response()->json(['products' => $products, 'subtotal' => $subtotal, 'itemsCount' => $itemsCount]);
    }

    public function getCartItems()
    {
        return response()->json(['cartItems' => \Cart::getTotalQuantity()]);
    }

    public function destroyCart()
    {
        \Cart::clear();
        return response()->json(['products' => null, 'subtotal' => 0, 'itemsCount' => 0]);
    }

    public function checkoutIndex()
    {
        if (\Cart::isEmpty()) {
            return redirect(route('cart.index'));
        }
        $page = Page::where('type','checkout')->where('status',Page::STATUS_ACTIVE)->first();
        return view('cart.checkout',compact('page'));
    }

    public function checkoutSubmit(Request $request)
    {
        $address = '';
        $date = '';
        $time = '';
        foreach ($request->keys() as $key) {
            if (strpos(strtolower($key), 'address') !== false) {
                $address = $request->get($key);
            }
            if (strpos(strtolower($key), 'date') !== false) {
                $date = $request->get($key);
            }
            if (strpos(strtolower($key), 'time') !== false) {
                $time = $request->get($key);
            }
        }
        $email = $request->get('email');
        $name = $request->get('name');
        $phone = $request->get('phone');
        $delivery_type = $request->get('deliveryType');
        $comments = $request->get('comment');
        $total = \Cart::getTotal();
        if ($total > 0) {
            $order = new Order();
            $order->user_email = $email;
            $order->user_name = $name;
            $order->user_phone = $phone;
            $order->order_comments = $comments;
            $order->user_address = $address;
            $order->delivery_type = $delivery_type;
            $order->delivery_date = $date . ' ' . $time;
            $order->amount = $total;
            $order->confirmed = true;
            $order->save();

            $products = \Cart::getContent();

            foreach ($products as $product) {
                $orderProduct = new OrderProduct();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $product['id'];
                $orderProduct->product_count = $product['quantity'];
                $orderProduct->product_price = $product['price'];
                $orderProduct->save();
            }

            if ($order->confirmed) {
                $orderedProducts = [];
                foreach ($order->orderProducts as $orderProduct) {
                    $product = Product::find($orderProduct->product_id);
                    $product->setAttribute('qty', $orderProduct->product_count);
                    $product->setAttribute('product_price', $orderProduct->product_price);
                    $orderedProducts[] = $product;
                }

                $users = User::where('role_id', 1)->select('email')->get()->pluck('email')->toArray();
                $firstUser = $users[0];
                if (($key = array_search($firstUser, $users)) !== false) {
                    unset($users[$key]);
                }

                \Mail::send('emails.newOrder', ['order' => $order, 'products' => $orderedProducts], function ($m) use ($users, $firstUser) {
                    $m->to($firstUser)
                        ->cc($users)
                        ->subject('Новый заказ с сайта Catering');
                });

                \Mail::send('emails.thanksOrder', ['order' => $order, 'products' => $orderedProducts], function ($m) use ($order) {
                    $m->to($order->user_email)
                        ->subject('Ваш заказ с сайта Catering.kz');
                });
            }

            \Cart::clear();
            return redirect()->route('cart.checkout.thanks', $order->id);

        } else {
            return redirect()->route('cart.index');
        }

    }

    public function checkoutThanks($orderId)
    {
        $order = Order::find($orderId);
        if (!$order->confirmed) {
            return redirect()->route('cart.checkout');
        }

        \Cart::clear();

        $orderedProducts = [];
        foreach ($order->orderProducts as $orderProduct) {
            $product = Product::find($orderProduct->product_id);
            $product->setAttribute('qty', $orderProduct->product_count);
            $product->setAttribute('product_price', $orderProduct->product_price);
            $orderedProducts[] = $product;
        }
        $page = Page::where('type','checkout')->where('status',Page::STATUS_ACTIVE)->first();

        return view('cart.thanks', compact('order', 'orderedProducts','page'));
    }
}
