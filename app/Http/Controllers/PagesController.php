<?php

namespace App\Http\Controllers;

use App\Advantage;
use App\Album;
use App\Category;
use App\Contact;
use App\Page;
use App\Partner;
use App\Post;
use App\Product;
use App\Review;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    public function index()
    {
        $page = Page::where('type', 'home')->first();
        $advantages = Advantage::where('active', true)->orderBy('sort_id')->get();
        $services = Service::where('active', true)->orderBy('sort_id')->get();
        $sets = Product::with('category')->where('featured', true)->where('active', true)->orderBy('sort_id')->get();
        $reviews = Review::where('active', true)->orderBy('sort_id')->take(3)->get();
        $partners = Partner::where('active', true)->orderBy('sort_id')->get();
        $posts = Post::where('status',Post::PUBLISHED)->orderBy('featured','DESC')->orderBy('date','DESC')->take(4)->get();
        $album = Album::where('active',true)->where('featured',true)->orderBy('sort_id')->first();
        return view('pages.index', compact('page', 'advantages', 'services', 'sets', 'reviews', 'partners','posts','album'));
    }

    public function search(Request $request)
    {
        dd('asdksajkd');
        $input = $request->get('input');
//        $categories = Category::search($input)->whereNull('parent_id')->where('active',true)->select('name', 'slug')->get();
        $products = Product::search($input)->where('active',true)->select('name', 'category_id')->with('category')->get();
//        $pages = Page::search($input)->where('status',Page::STATUS_ACTIVE)->select('title', 'slug')->get();
//        $posts = Post::search($input)->where('status',Post::PUBLISHED)->select('title', 'slug')->get();
//        $services = Service::search($input)->where('active',true)->select('title', 'slug')->get();

        $collection = collect($products);
        foreach ($collection as $item) {
            switch (class_basename($item)) {
                case 'Category':
                    $item->setAttribute('full_link', route('pages.inner',['menu',$item]));
                    $item->setAttribute('item', 'Каталог');
                    break;
                case 'Product':
                    $item->setAttribute('full_link',route('pages.inner',['menu',$item->category->slug]));
                    $item->setAttribute('item', 'Товар');
                    break;
                case 'Page':
                    $item->setAttribute('full_link', route('pages.show',$item));
                    $item->setAttribute('name', $item->title);
                    $item->setAttribute('item', 'Страница');
                    break;
                case 'Service':
                    $item->setAttribute('full_link', route('pages.inner',['uslugi',$item]));
                    $item->setAttribute('name', $item->title);
                    $item->setAttribute('item', 'Услуга');
                    break;
            }
        }

        return view('pages.search');
        return response()->json(['items' => $collection]);
    }
    public function show(Page $page)
    {
        switch ($page->type) {
            case 'about':
                return view('pages.about', compact('page'));
                break;
            case 'services':
                $services = Service::where('active', true)->orderBy('sort_id')->get();
                return view('services.index', compact('page', 'services'));
                break;
            case 'contacts':
                $map = Contact::where('active', true)->where('type', 'map')->first()->value;
                $contactPhones = Contact::where('active', true)->where('type', 'phone')->orderBy('sort_id')->get();
                return view('pages.contacts', compact('page', 'map', 'contactPhones'));
                break;
            case 'gallery':
                $albums = Album::where('active', true)->orderBy('sort_id')->paginate(16);
                return view('albums.index', compact('page', 'albums'));
                break;
            case 'reviews':
                $reviews = Review::where('active', true)->orderBy('sort_id')->paginate(6);
                return view('pages.reviews', compact('page', 'reviews'));
                break;
            case 'news':
                $posts = Post::where('status', Post::PUBLISHED)->orderBy('featured', 'DESC')->orderBy('date', 'DESC')->paginate(6);
                return view('posts.index', compact('page', 'posts'));
                break;
            case 'catalog':
                $categories = Category::where('active', true)->where('parent_id', null)->orderBy('order')->get();
                return view('products.catalog', compact('page', 'categories'));
                break;
            case 'other':
                $allServices = Service::where('active', true)->orderBy('sort_id')->get();
                return view('pages.show', compact('page', 'allServices'));
                break;
            default:
                return view('pages.show', compact('page'));
                break;
        }
    }

    public function showInner(Page $page, $slug)
    {
        switch ($page->type) {
            case 'services':
                $service = Service::where('active', true)->where('slug', $slug)->first();
                if ($service) {
                    return view('services.show', compact('page', 'service'));
                } else {
                    abort(404);
                }
                break;
            case 'news':
                $post = Post::where('status', Post::PUBLISHED)->where('slug', $slug)->first();
                if ($post) {
                    return view('posts.show', compact('page', 'post'));
                } else {
                    abort(404);
                }
                break;
            case 'gallery':
                $album = Album::where('active', true)->where('slug', $slug)->first();
                if ($album) {
                    return view('albums.show', compact('page', 'album'));
                } else {
                    abort(404);
                }
                break;
            case 'catalog':
                $category = Category::where('active', true)->whereNull('parent_id')->where('slug', $slug)->firstOrFail();
                if ($category) {
                    return view('products.index', compact('page', 'category'));
                } else {
                    abort(404);
                }
                break;
            default:
                abort(404);
                break;
        }
    }

    public function getCurrentCategory($slug)
    {
        $category = Category::with('parent')->where('slug', $slug)->first();
        return response()->json(['category' => $category]);
    }

    public function getAjaxProducts($categoryId)
    {
        $categoryIds = Category::where('parent_id', $categoryId)
            ->pluck('id')
            ->push($categoryId)
            ->all();
        $products = Product::with('category')->whereIn('category_id', $categoryIds)->where('active', true)->orderBy('sort_id')->get();
        if ($products->count()) {
            $tags = collect([]);
            foreach ($products as $product) {
                $product->getAttributes();
                $product->setAttribute('image_link', \Voyager::image($product->thumbnail('big', 'image')));
                $product->setAttribute('webp_link', $product->webpImage);
                $product->setAttribute('web_link', $product->link);
                $product->setAttribute('filterCategory', $product->category->name);
                $filter_tags = collect(explode(';',$product->tags));
                $filtereds = collect([]);
                foreach ($filter_tags as $filter_tag){
                    if (strlen($filter_tag) > 1) {
                        $filtereds->push($filter_tag);
                    }
                }
                $tags = $tags->merge($filtereds);
                if ($product->tags != null) {
                    foreach (explode(';',$product->tags) as $tag) {
                        if ($tag && strlen($tag) > 1){
                            $product->setAttribute($tag, true);
                        }
                    }
                }
            }
            $tags = $tags->unique();

            $sizes = $products->groupBy('size')->keys();
            $weights = $products->groupBy('weight')->keys();
            $categories = $products->groupBy('filterCategory')->keys();

            $result_options = [];
            if (count($sizes) > 1 && $sizes[0] != ""){
                foreach ($sizes as $size) {
                    if (strlen($size) > 0) {
                        $result_options['size'][$size] = false;
                    }
                }
            }
            if (count($tags) > 1 && $tags[0] != "") {
                foreach ($tags as $tag) {
                    if (strlen($tag) > 0) {
                        $result_options['tag'][$tag] = false;
                    }
                }
            }
            if (count($categories) > 1 && $categories[0] != "") {
                foreach ($categories as $cat) {
                    if (strlen($cat) > 0) {
                        $result_options['filterCategory'][$cat] = false;
                    }
                }
            }
            if (count($weights) > 1 && $weights[0] != "") {
                foreach ($weights as $weight) {
                    if (strlen($weight) > 0) {
                        $result_options['weight'][$weight] = false;
                    }
                }
            }

            foreach ($result_options as $k => $opt) {
                ksort($result_options[$k]);
                $optKeys = array_unique(array_keys($opt));
                foreach ($optKeys as $optKey) {
                    $result_options[$k][$optKey] = false;
                }
                if (count($result_options[$k]) < 2) {
                    unset($result_options[$k]);
                }
            }

            $result_options['price']['max'] = $products->max('price');
            $result_options['price']['min'] = $products->min('price');


            return response()->json(['products' => $products, 'sizes' => $sizes, 'weights' => $weights, 'tags'=>$tags,'filterCategories' => $categories, 'filters' => $result_options]);
        } else {
            return response()->json(['products' => null]);
        }
    }

    public function feedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false]);
        } else {
            $users = User::where('role_id', 1)->select('email')->get()->pluck('email')->toArray();
            $firstUser = $users[0];
            if (($key = array_search($firstUser, $users)) !== false) {
                unset($users[$key]);
            }

            $clientEmail = $request->has('email') ? $request->email : null;
            $page = array_key_exists('page', $request->all()) ? $request->page : null;
            $pageLink = array_key_exists('pageLink', $request->all()) ? $request->pageLink : null;
            $eventFormat = array_key_exists('eventFormat', $request->all()) ? $request->eventFormat : null;
            $date = array_key_exists('date', $request->all()) ? $request->date : null;
            $count = array_key_exists('count', $request->all()) ? $request->count : null;
            Mail ::send('emails.callback', [
                'name' => $request->name,
                'phone' => $request->phone,
                'clientEmail' => $clientEmail,
                'page' => $page,
                'pageLink' => $pageLink,
                'eventFormat' => $eventFormat,
                'date' => $date,
                'count' => $count,
            ], function ($m) use ($users, $firstUser) {
                $m->to($firstUser)
                    ->cc($users)
                    ->subject('Новая заявка на Обратный звонок');
            });
        }

        return redirect(route('thanks', ['name' => $request->name]));
    }

    public function thanks(Request $request)
    {
        if ($request->get('name')) {
            return view('pages.thanks');
        } else {
            return redirect()->back();
        }
    }
}
