<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ReviewsController extends Controller
{
    public function newReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            if (Review::where('email', $request->email)->exists() && $request->email != null) {
                return response()->json(['success' => false, 'message' => 'Вы уже оставляли отзыв']);
            } else {
                $review = new Review();
                $review->email = $request->get('email');
                $review->name = $request->get('name');
                $review->company = $request->get('company');
                $review->content = $request->get('content');
                $review->sort_id = Review::where('active', true)->orderBy('sort_id', 'DESC')->first()->sort_id + 1;
                $review->active = false;

                if ($request->hasFile('photo') && $request->photo != 'undefined') {
                    $path = 'reviews'.DIRECTORY_SEPARATOR.date('FY').DIRECTORY_SEPARATOR;
                    $file = $request->file('photo');
                    $filename = Str::random(10);
                    $image = Image::make($file);

                    $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();

                    $image = $image->resize(
                        1000,
                        null,
                        function (Constraint $constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        }
                    )->encode($file->getClientOriginalExtension(), 100);
                    $imageWeb = $image->resize(
                        1000,
                        null,
                        function (Constraint $constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        }
                    )->encode('webp', 100);
                    Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public');
                    Storage::disk(config('voyager.storage.disk'))->put($path.$filename.'.webp', (string) $imageWeb, 'public');

                    $review->image = $fullPath;
                }
                $review->save();
            }

            return response()->json(['success' => true]);
        }
    }
}
