<?php

namespace App\Providers;

use App\Contact;
use App\Seopage;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Http\Controllers\ContentTypes\Image;
use TCG\Voyager\Http\Controllers\ContentTypes\MultipleImage;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerController;
use TCG\Voyager\Http\Controllers\VoyagerSettingsController;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VoyagerBaseController::class, \App\Http\Controllers\Voyager\VoyagerBaseController::class);
        $this->app->bind(VoyagerController::class, \App\Http\Controllers\Voyager\VoyagerController::class);
        $this->app->bind(Controller::class, \App\Http\Controllers\Voyager\Controller::class);
        $this->app->bind(VoyagerSettingsController::class, \App\Http\Controllers\Voyager\VoyagerSettingsController::class);
        $this->app->bind(Image::class, \App\Http\Controllers\Voyager\ContentTypes\Image::class);
        $this->app->bind(MultipleImage::class, \App\Http\Controllers\Voyager\ContentTypes\MultipleImage::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        \Carbon\Carbon::setLocale('ru');

        view()->composer('*', function ($view)
        {
            $seo_page = Seopage::where('slug',\Request::fullUrl());
            if ($seo_page->exists()){
                $seoTitle = $seo_page->first()->meta_title ? $seo_page->first()->meta_title : '';
                $keywords = $seo_page->first()->meta_keywords ? $seo_page->first()->meta_keywords : '';
                $description = $seo_page->first()->meta_description ? $seo_page->first()->meta_description : '';
            }else {
                $seoTitle = '';
                $keywords = '';
                $description = '';
            }
            $view->with('seoTitle', $seoTitle);
            $view->with('keywords', $keywords);
            $view->with('description', $description);

            $graph = Contact::where('type','graph')->where('is_main',true)->where('active',true)->orderBy('sort_id')->first();
            $view->with('graph',$graph);

            $phones = Contact::where('type','phone')->where('is_main',true)->where('active',true)->orderBy('sort_id')->get();
            $view->with('phones',$phones);

            $email = Contact::where('type','email')->where('is_main',true)->where('active',true)->orderBy('sort_id')->first();
            $view->with('email',$email);

            $address = Contact::where('type','address')->where('is_main',true)->where('active',true)->orderBy('sort_id')->first();
            $view->with('address',$address);

            $map = Contact::where('type','map')->where('is_main',true)->where('active',true)->orderBy('sort_id')->first();
            $view->with('map',$map);

            $contacts = Contact::where('is_main',true)->where('active',true)->orderBy('sort_id')->get()->groupBy('city');
            $view->with('contacts',$contacts);
        });
    }
}
