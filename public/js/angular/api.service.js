angular.module('catering')
    .factory('Api', function ($http) {
        return {
            getCategoryProducts: function (categoryId) {
                return $http.get('/catalog/' + categoryId + '/products', {
                    categoryId: categoryId
                });
            },
            getCurrentCategory: function (slug) {
                return $http.get('/catalog/getCurrent/' + slug);
            },
            getCerts: function (input) {
                return $http({url:'/certs',method:"GET",params: {
                    input: input
                }});
            },
            getCurrentProduct: function (slug) {
                return $http.get('/product/getCurrent/' + slug);
            },
            addToCart: function (productId, qty) {
                return $http.post('/cart/addToCart/'+productId, {
                    productId: productId,
                    qty: qty
                });
            },
            getCartItems: function () {
                return $http.get('/cart/getCartItems');
            },
            getCartContent: function () {
                return $http.get('/cart/getCartContent');
            },
            updateCart: function (itemId,itemQty) {
                return $http.post('/cart/update', {
                    itemId: itemId,
                    itemQty:itemQty
                });
            },
            clearCart: function () {
                return $http.get('/cart/clear');
            },
            removeFromCart: function (itemId) {
                return $http.post('/cart/remove', {
                    itemId: itemId
                });
            },
            searchByInput: function (input) {
                return $http({url:'/search',method:"GET",params: {
                    input: input
                }});
            }

        };

    });