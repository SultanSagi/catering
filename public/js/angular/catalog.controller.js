(function (catering) {
    "use strict";
    catering.controller('CatalogController', ['$rootScope', '$scope', '$window', '$http', '$timeout', 'Api', function ($rootScope, $scope, $window, $http, $timeout, Api) {
        var cat = {
            products: [],
            options_key: '',
            options: '',
            brands: '',
            filterExpression: {},
            loading: false,
            filters: {
                price: {
                    min: 0,
                    max: 10000,
                    options: {
                        floor: 0,
                        ceil: 10000,
                        translate: function (value) {
                            return value + ' ₸';
                        }
                    }
                },
                isEmpty: true
            },
            categories: [],
            pageSize: 12,
            currentPage: 1,
            filtered_products: [],
            currentCategory: {},
            initFunctions: function (slug) {
                cat.loading = true;
                Api.getCurrentCategory(slug).then(function (response) {
                    if (response.data) {
                        cat.currentCategory = response.data.category;
                        cat.getCategoryProducts(cat.currentCategory.id);
                    }
                });
            },
            checkForMin: function (product) {
                setTimeout(function () {
                    if (product.qty < product.min_value || !product.qty){
                        product.qty = product.min_value;
                    }
                },500);
            },
            refreshSlider: function () {
                $timeout(function () {
                    $rootScope.$broadcast('rzSliderForceRender');
                }, 10);
            },
            addToCart: function (product, qty,$event) {
                if (qty < product.min_value || qty == 'undefined'){
                    alert('Минимальное количество данного товара:'+product.min_value + 'шт');
                }else {
                    var cartCountValue = hc.cartItems;
                    var cartCount = $('#shopCart .count');
                    var cartBtn = $($event.currentTarget);
                    var cartCountPosition = $(cartCount).offset();
                    var btnPosition = $($event.currentTarget).offset();
                    var leftPos =
                        cartCountPosition.left < btnPosition.left
                            ? btnPosition.left - (btnPosition.left - cartCountPosition.left)
                            : cartCountPosition.left;
                    var topPos =
                        cartCountPosition.top < btnPosition.top
                            ? cartCountPosition.top
                            : cartCountPosition.top;

                    if (!$(cartBtn).parents('.modal').length){
                        $('body')
                            .append("<span class='counter-pos'>"+qty+"</span>");
                        $('body').find(".counter-pos").offset({ top: btnPosition.top, left: btnPosition.left });
                        $('body').find(".counter-pos").each(function(i,count){
                            $(count).offset({
                                left: leftPos,
                                top: topPos
                            })
                                .animate(
                                    {
                                        opacity: 0
                                    },
                                    800,
                                    function() {
                                        $(this).remove();
                                    }
                                );
                            $('#shopCart').addClass('shakeit');
                        });
                    }else {
                        $(cartBtn)
                            .append("<span class='counter'>"+qty+"</span>");
                        $(cartBtn).find(".counter").each(function(i,count){
                            $(count).offset({
                                left: leftPos,
                                top: topPos
                            })
                                .animate(
                                    {
                                        opacity: 0
                                    },
                                    800,
                                    function() {
                                        $(this).remove();
                                    }
                                );
                            $('#shopCart').addClass('shakeit');
                        });
                    }

                    product.animating = false;
                    if (!product.animating) {
                        //animate if not already animating
                        product.animating = true;
                        $($event.currentTarget).addClass('added').find('path').eq(0).animate({
                            //draw the check icon
                            'stroke-dashoffset': 0
                        }, 300, function () {
                            setTimeout(function () {
                                $($event.currentTarget).removeClass('added').find('em').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                                    //wait for the end of the transition to reset the check icon
                                    $($event.currentTarget).find('path').eq(0).css('stroke-dashoffset', '19.79');
                                    product.animating  = false;
                                });

                                if ($('.no-csstransitions').length > 0) {
                                    // check if browser doesn't support css transitions
                                    $($event.currentTarget).find('path').eq(0).css('stroke-dashoffset', '19.79');
                                    product.animating  = false;
                                }
                            }, 600);
                        });

                        setTimeout(function () {
                            $($event.currentTarget).find('path').eq(0).css('stroke-dashoffset', '19.79');
                            product.animating = false;
                            $('#shopCart').removeClass('shakeit');
                        },600);
                    }

                    Api.addToCart(product.id, qty).then(function (response) {
                        if (response.data) {
                            product.inCart = true;
                            hc.getCartItems();
                            cart.getCartContent();

                        }
                    });
                }

            },
            refreshCategory: function (category) {
                window.location.href = category;
            },
            getCategoryProducts: function (categoryId) {
                Api.getCategoryProducts(categoryId).then(function (response) {
                    if (response.data && response.data.products) {
                        cat.loading = false;
                        cat.products = response.data.products;
                        angular.forEach(cat.products, function (item) {
                            // item.price = cat.formatMoney(item.price);
                            item.rating_array = [].constructor(item.rating);
                            item.qty = item.min_value;
                        });
                        cat.filters.isEmpty = false;
                        cat.sizes = response.data.sizes;
                        cat.weights = response.data.weights;
                        cat.filterCategories = response.data.filterCategories;
                        cat.filters = response.data.filters;
                        angular.forEach(cat.filters, function (filter, key) {
                            if (key === 'price') {
                                cat.filters[key] = {
                                    min: filter.min,
                                    max: filter.max,
                                    options: {
                                        floor: 0,
                                        ceil: filter.max,
                                        translate: function (value) {
                                            return value + ' ₸';
                                        }
                                    }
                                };
                                cat.refreshSlider();
                            } else {
                                filter.filtersLimit = 4;
                            }
                        });
                    } else {
                        cat.products = [];
                        cat.loading = false;
                    }
                });
            },

            changePageSize: function (pageSize) {
                cat.pageSize = pageSize;
            },
            numberOfPages: function () {
                return Math.ceil(cat.products.length / cat.pageSize);
            },
            updateFilters: function (item) {

                var validCounter = 0;
                var trueCounter;
                for (var categoryKey in cat.filters) {
                    trueCounter = 0;
                    for (var valueKey in cat.filters[categoryKey]) {
                        if (valueKey == 'filtersLimit') {
                            continue;
                        }

                        if (cat.filters[categoryKey][valueKey] || !cat.filters[categoryKey]) {
                            trueCounter = 1;

                            if (categoryKey == 'price' && cat.between(item[categoryKey], cat.filters[categoryKey].min, cat.filters[categoryKey].max)) {
                                trueCounter = -1;
                                break;
                            }
                            if (item[categoryKey] === valueKey) {
                                trueCounter = -1;
                                break;
                            }
                            if (item[valueKey]) {
                                trueCounter = -1;
                                break;
                            }
                        }


                    }
                    validCounter += +(trueCounter < 1);
                }
                return validCounter === Object.keys(cat.filters).length;
            },
            showMoreFilter: function (category) {
                return (category.filtersLimit + 1 < Object.keys(category).length) && Object.keys(category).length > 5;
            },
            formatMoney: function (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
            },
            between: function (x, min, max) {
                return x >= min && x <= max;
            },
            minus: function (product) {
                if (product.min_value < product.qty) {
                    product.qty--;
                }
            },
            plus: function (product) {
                product.qty++;
            },

        };

        window.cat = cat;
        angular.extend(cat, this);
        return cat;

    }]).filter('filtersLimitTo', [function () {
        return function (obj, limit) {
            var keys = Object.keys(obj);
            if (keys.length < 1) {
                return [];
            }
            var ret = new Object,
                count = 0;
            angular.forEach(keys, function (key, arrayIndex) {
                if (count >= limit) {
                    return false;
                }
                ret[key] = obj[key];
                count++;
            });
            return ret;
        };
    }]).directive("owlCarousel", function() {
        return {
            restrict: 'E',
            transclude: false,
            link: function (scope) {
                scope.initCarousel = function(element) {
                    // provide any default options you want
                    var defaultOptions = {
                    };
                    var customOptions = scope.$eval($(element).attr('data-options'));
                    // combine the two options objects
                    for(var key in customOptions) {
                        defaultOptions[key] = customOptions[key];
                    }
                    // init carousel
                    var curOwl = $(element).data('owlCarousel');
                    if(!angular.isDefined(curOwl)) {
                        $(element).owlCarousel(defaultOptions);
                    }
                    scope.cnt++;
                };
            }
        };
    })
        .directive('owlCarouselItem', [function() {
            return {
                restrict: 'A',
                transclude: false,
                link: function(scope, element) {
                    // wait for the last item in the ng-repeat then call init
                    if(scope.$last) {
                        scope.initCarousel(element.parent());
                    }
                }
            };
        }]);
})(catering);