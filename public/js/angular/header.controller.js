(function (catering) {
    "use strict";
    catering.controller('HeaderController', function ($scope, Api) {
        var hc = {
            cartItems: 0,
            compareItems: 0,
            cartContent: {},
            cartSubtotal: 0,
            init: function () {
                hc.getCartItems();
            },
            getCartItems: function () {
                Api.getCartItems().then(function (response) {
                    if (response.data) {
                        hc.cartItems = response.data.cartItems;

                        var shopCart = document.getElementById('shopCart');
                        if (shopCart) {
                            if (hc.cartItems > 0) {
                                shopCart.classList.add("show");
                                shopCart.classList.remove("hide");
                            } else {
                                shopCart.classList.add("hide");
                                shopCart.classList.remove("show");
                            }

                            if (shopCart.className === 'show') {
                                setTimeout(function () {
                                    shopCart.style.opacity = 1;
                                    shopCart.style.height = '50px';
                                }, 0); // timed to occur immediately
                            }
                            if (shopCart.className === 'hide') {
                                setTimeout(function () {
                                    shopCart.style.opacity = 0;
                                    shopCart.style.height = 0;

                                }, 700); // timed to match animation-duration
                            }
                        }

                    }
                });
            },
            getCartContent: function () {
                Api.getCartContent().then(function (response) {
                    if (response.data) {
                        hc.cartContent = response.data.products;
                        hc.cartSubtotal = response.data.subtotal;
                        hc.cartItems = response.data.itemsCount;
                    }
                });
            },
            deleteItemCart: function (product) {
                angular.forEach(hc.cartContent, function (item) {
                    if (item === product) {
                        item.qty = 0;
                    }
                });
                Api.removeFromCart(product.id).then(function (response) {
                    if (response.data) {
                        cart.getResponseData(response.data);
                    }
                });
            }
        };
        window.hc = hc;
        angular.extend(hc, this);

        return hc;
    });
    catering.controller('SearchController', function ($scope, Api) {
        var sc = {
            searchItems: [],
            searchByInput: function (input) {
                if (input.length < 2) return;
                Api.searchByInput(input).then(function (response) {
                    if (response.data) {
                        sc.searchItems = response.data.items;
                        $('#searchForm').find('.items').addClass('open');
                    }
                });
            }
        };
        window.sc = sc;
        angular.extend(sc, this);

        return sc;
    });
})(catering);