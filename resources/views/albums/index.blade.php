@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','albums-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title])
    <h1 class="titleDark">{{$page->title}}</h1>
</div>
<section id="gallery">
    <div class="container">
        <div class="row">
            @foreach ($albums as $album)
                <div class="col-lg-4 col-md-6 p-2">
                    <div class="galleryItem">
                        <div class="galleryItemImg">
                            <a href="{{ route('pages.inner',[$page,$album->slug])}}">
                                <picture>
                                    <source srcset="{{$album->webpImage}}" type="image/webp">
                                    <source srcset="{{$album->bigThumb}}" type="image/pjpeg">
                                    <img src="{{$album->bigThumb}}" alt="{{$album->seo_title}}"
                                         class="img-fluid">
                                </picture>
                            </a>
                        </div>
                        <div class="galleryItemCaption">
                            <div class="row">
                                <div class="col-6"><span>{{$album->gallery ? count(json_decode($album->gallery)) : '0'}}</span></div>
                                <div class="col-6"><time datetime="{{ $album->date ? $album->date->translatedFormat('Y-m-d') : $album->created_at->translatedFormat('Y-m-d')}}">{{ $album->date ? $album->date->translatedFormat('d F Y') : $album->created_at->translatedFormat('d F Y')}}</time></div>
                            </div>
                            <h2><a href="{{route('pages.inner',[$page,$album->slug])}}">{{ $album->title }}</a></h2>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row pt-4">
            {!! $page->body !!}
        </div>
    </div>
</section>
@endsection