@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $album->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $album->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$album->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $album->meta_description))
@section('image',$album->thumbic)
@section('url',url()->current())
@section('page_class','album-page')
@section('content')
    <div class="breadcrumbs">
        @include('partials.breadcrumbs',['title'=>$page->title,'titleLink'=>route('pages.show',$page),'subtitle'=>$album->title])
        <h1 class="titleDark">{{$album->title}}</h1>
    </div>
    <section id="galleryProduct">
        <div class="container">
            <div class="row">
                @foreach (json_decode($album->gallery) as $photo)
                    <div class="col-lg-3 col-md-4 col-sm-6 p-2">
                        <div class="galleryProductItem {{\App\Album::mimeType(public_path('storage/').$photo) ? 'photo' : 'video'}}">
                            @if(\App\Album::mimeType(public_path('storage/').$photo))
                                <a data-fancybox="gallery" href="{{ Voyager::image($photo) }}">
                                    <picture>
                                        <source srcset="{{str_replace('.'.pathinfo(Voyager::image($photo),PATHINFO_EXTENSION),'-small.webp',Voyager::image($photo))}}"
                                                type="image/webp">
                                        <source srcset="{{Voyager::image($album->getThumbnail($photo,'small'))}}"
                                                type="image/pjpeg">
                                        <img src="{{Voyager::image($album->getThumbnail($photo,'small'))}}"
                                             alt="{{$album->seo_title}}">
                                    </picture>
                                </a>
                            @else
                                <a data-fancybox="gallery" href="{{ Voyager::image($photo) }}">
                                    <video>
                                        <source src="{{Voyager::image($photo)}}" type="video/mp4">
                                        <source src="{{Voyager::image($photo)}}" type="video/ogg">
                                        Your browser does not support the video tag.
                                    </video>
                                </a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection