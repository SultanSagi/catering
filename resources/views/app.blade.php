<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials.head')
<body class="@yield('page_class')" data-ng-app="catering">
@include('partials.header')
@yield('content')
@include('partials.footer')
@include('partials.modals')
@include('partials.scripts')
</body>
</html>