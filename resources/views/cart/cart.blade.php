@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','albums-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title])
    <h1 class="titleDark">{{$page->title}}</h1>
</div>
<section id="cart" data-ng-controller="CartController as cart">
    <div class="container" data-ng-init="cart.initCart()">
        <div class="row">
            <div class="col-md-4 col-lg-3 pr-4 order-last order-md-first mt-3 mt-md-0" data-ng-if="cart.cartItemsCount > 0">
                <aside>
                    <div class="cartTotal">
                        <div class="row">
                            <div class="col-12 text-center text-sm-left col-sm-6 col-md-12">
                                <p>Общая стоимость:</p>
                            </div>
                            <div class="col-12 col-sm-6 text-center text-sm-right text-md-left col-md-12">
                                <p class="total"><span>@{{ cart.total }}</span> тенге</p>
                            </div>
                            <div class="col-12 text-center text-md-left">
                                <a href="{{route('cart.checkout')}}" class="btnDark">Оформить заказ</a>
                            </div>
                        </div>
                    </div>
                    <div class="cartSteps d-none d-md-block">
                        <ul class="list-unstyled">
                            <li class="active"><span class="stepNumber">1</span><span class="step">Корзина</span></li>
                            <li><span class="stepNumber">2</span><span class="step">Оформление заказа</span></li>
                            <li><span class="stepNumber">3</span><span class="step">Заказ принят</span></li>
                        </ul>
                    </div>
                </aside>
                <div class="anyQuest d-none d-md-block">
                    <h3>Остались вопросы?</h3>
                    <a href="{{$phones->first()->link}}">{{$phones->first()->value}}</a>
                    <p>{{$graph->value}}</p>
                    <small>Затрудняетесь с выбором? Поможем с подбором меню!</small>
                </div>
            </div>
            <div class="col-md-8 col-lg-9 productWrap" data-ng-if="cart.cartItemsCount > 0">
                <div class="cartProduct row align-items-center" data-ng-repeat="product in cart.products">
                    <div class="cartImage col-3 py-2 col-lg-auto px-2">
                        <img class="img-fluid" src="#" data-ng-src="@{{product.attributes.image_link}}" alt="@{{ product.name }}">
                    </div>
                    <div class="cartName col-8 py-2 col-lg-4 px-2">
                        <p class="m-0">@{{ product.name }}</p>
                    </div>
                    <div class="cartOptions col-3 py-3 col-lg px-2 justify-self-center">
                        <span>@{{ product.quantity }} шт</span>
                        <span>@{{ product.attributes.weight }} грамм</span>
                    </div>
                    <div class="col-4 py-3 col-lg-auto count px-2 justify-content-center">
                        {{--<input type="button" value="-" class="minusBtn">--}}
                        <a href="javascript:void(0);" class="minusBtn" data-ng-click="cart.removeQuantity(product)">-</a>
                        <input type="text" value="#" class="qtyInput" data-ng-change="cart.checkForMin(product)" data-ng-blur="cart.checkForMin(product)" min="@{{ product.attributes.min_value }}" data-ng-value="@{{product.quantity}}"
                               data-ng-model="product.quantity">
                        {{--<input type="button" value="+" class="plusBtn">--}}
                        <a href="javascript:void(0);" class="plusBtn" data-ng-click="cart.addQuantity(product)">+</a>
                    </div>
                    <div class="cartSubtotal col-3 col-lg px-2 py-3">
                        <p class="price m-0 text-center">@{{ product.subtotal }} тг</p>
                    </div>
                    <div class="col-2 text-center py-3 col-lg-auto px-2 removeItem">
                        <a href="javascript:void(0);" data-ng-click="cart.deleteItem(product)" class="btn btn-remove">X</a>
                    </div>
                </div>
            </div>
            <p class="basket" ng-if="cart.cartItemsCount < 1">Корзина пустая</p>
        </div>
    </div>
</section>
@endsection
