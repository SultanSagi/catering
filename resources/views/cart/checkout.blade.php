@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','albums-page')
@section('content')
    <div class="breadcrumbs">
        @include('partials.breadcrumbs',['title'=>$page->title])
        <h1 class="titleDark">{{$page->title}}</h1>
    </div>
    <section id="cart" data-ng-controller="CartController as cart">
        <div class="container" data-ng-init="cart.initCart()">
            <div class="row">
                <div class="col-md-4 col-lg-3 pr-4 order-last order-md-first mt-3 mt-md-0 d-none d-md-block">
                    <aside>
                        <div class="cartTotal">
                            <div class="row">
                                <div class="col-12 text-center text-sm-left col-sm-6 col-md-12">
                                    <p>Общая стоимость:</p>
                                </div>
                                <div class="col-12 col-sm-6 text-center text-sm-right text-md-left col-md-12">
                                    <p class="total"><span>@{{ cart.total }}</span> тенге</p>
                                </div>
                                <div class="col-12 text-center text-md-left">
                                    <a href="" class="btnDark">Оформить заказ</a>
                                </div>
                            </div>
                        </div>
                        <div class="cartSteps d-none d-md-block">
                            <ul class="list-unstyled">
                                <li><span class="stepNumber">1</span><span class="step">Корзина</span></li>
                                <li class="active"><span class="stepNumber">2</span><span
                                            class="step">Оформление заказа</span></li>
                                <li><span class="stepNumber">3</span><span class="step">Заказ принят</span></li>
                            </ul>
                        </div>
                    </aside>
                    <div class="anyQuest d-none d-md-block">
                        <h3>Остались вопросы?</h3>
                        <a href="{{$phones->first()->link}}">{{$phones->first()->value}}</a>
                        <p>{{$graph->value}}</p>
                        <small>Затрудняетесь с выбором? Поможем с подбором меню!</small>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 productWrap">
                    <form action="{{route('cart.checkout.submit')}}" method="post">
                        @csrf
                    <div class="toggle-bg">
                        <input type="radio" data-ng-model="cart.deliveryType" name="deliveryType" value="delivery">
                        <input type="radio" data-ng-model="cart.deliveryType" name="deliveryType" value="self">
                        <span class="switch"></span>
                    </div>

                        <div data-ng-show="cart.deliveryType == 'delivery'">
                            <p class="m-0">Дата и время доставки*</p>
                            <small class="d-block mb-3">Рекомендуем выбирать время доставки с запасом до начала
                                мероприятия
                            </small>
                        </div>
                        <div class="dates" data-ng-show="cart.deliveryType == 'delivery'">
                            <input id="date" type="text" data-ng-required="cart.deliveryType == 'delivery'" class="datepickerHere mb-3 mb-md-0" name="date"
                                   placeholder="Выберите дату" autocomplete="off"/>
                            <input type="text" name="time" id="time" class="time" data-ng-required="cart.deliveryType == 'delivery'"
                                   placeholder="00:00">
                        </div>
                        <div class="address" data-ng-show="cart.deliveryType == 'delivery'">
                            <p class="pt-4">Адрес доставки*</p>
                            <input id="address" type="text" name="address" class="d-block w-100"
                                   placeholder="г. Алматы, ул. Калдаякова 154 " data-ng-required="cart.deliveryType == 'delivery'">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="pt-4">Электронная почта*</p>
                                <input id="email" type="email" name="email" class="" placeholder="mail@example.com" required>
                            </div>
                            <div class="col-sm-6">
                                <p class="pt-4">Номер телефона*</p>
                                <input id="phone" type="tel" name="phone" class="phonee" placeholder="+7 (777) 777 77 77" required>
                            </div>
                        </div>
                        <p class="pt-4">Представьтесь пожалуйста*</p>
                        <input id="name" type="text" name="name" class="d-block w-100" placeholder="Аскар Аскарович" required>
                        <p class="pt-4 info showComment" data-ng-click="cart.showCommentInput()">Указать дополнительную информацию</p>
                        <textarea name="comment" data-ng-show="cart.showComment" id="comment" cols="30" rows="6" placeholder="Дополнительная информация, комментарии к заказу ..."></textarea>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="submit" class="submit">Подтвердить заказ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
