@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','albums-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title])
    <h1 class="titleDark">{{$page->title}}</h1>
</div>
<section id="cart">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-3 pr-4 order-last order-md-first mt-3 mt-md-0 d-none d-md-block">
                <aside>
                    <div class="cartTotal">
                        <div class="row">
                            <div class="col-12 text-center text-sm-left col-sm-6 col-md-12">
                                <p>Общая стоимость:</p>
                            </div>
                            <div class="col-12 col-sm-6 text-center text-sm-right text-md-left col-md-12">
                                <p class="total"><span>{{number_format($order->amount,0,'',' ')}}</span> тенге</p>
                            </div>
                            <div class="col-12 text-center text-md-left">
                                <a href="" class="btnDark">Оформить заказ</a>
                            </div>
                        </div>
                    </div>
                    <div class="cartSteps d-none d-md-block">
                        <ul class="list-unstyled">
                            <li><span class="stepNumber">1</span><span class="step">Корзина</span></li>
                            <li><span class="stepNumber">2</span><span class="step">Оформление заказа</span></li>
                            <li class="active"><span class="stepNumber">3</span><span class="step">Заказ принят</span></li>
                        </ul>
                    </div>
                </aside>
                <div class="anyQuest d-none d-md-block">
                    <h3>Остались вопросы?</h3>
                    <a href="{{$phones->first()->link}}">{{$phones->first()->value}}</a>
                    <p>{{$graph->value}}</p>
                    <small>Затрудняетесь с выбором? Поможем с подбором меню!</small>
                </div>
            </div>
            <div class="col-md-8 col-lg-9 productWrap">
                <div class="thankYou">
                    <div class="row">
                        <div class="col-12 pb-3 text-center">
                            <img src="{{ asset('img/sps.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="col-12 text-center">
                            <h2>Спасибо!<br>Ваш заказ принят</h2>
                            <p class="mb-5">В ближайшее время мы свяжемся с Вами!</p>
                            <a href="{{route('pages.show',['menu'])}}" class="btnLight arrowLeft">Вернуться в каталог?</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection