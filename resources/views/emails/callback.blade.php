<!DOCTYPE HTML>
<html>
<head>
    <title>Новая заявка с сайта</title>
</head>
<body>

<h1>Новая заявка с сайта</h1>
<br>
<p>Имя отправителя : {{$name}}</p>
<p>Телефон : {{$phone}}</p>
@isset($clientEmail)
    <p>Email : {{$clientEmail}}</p>
@endisset

@isset($eventFormat)
    <p>Формат событии : {{$eventFormat}}</p>
@endisset
@isset($date)
    <p>Дата событии : {{$date}}</p>
@endisset
@isset($count)
    <p>Кол-во персон : {{$count}}</p>
@endisset

@isset($page)
    <p>Со страницы : {{$page}}</p>
@endisset

@isset($pageLink)
    <p>Ссылка на страницу : <a href="{{$pageLink}}">{{$pageLink}}</a></p>
@endisset

</body>
</html>