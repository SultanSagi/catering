<!DOCTYPE HTML>
<html>
<head>
    <title>Новый заказ с сайта</title>
</head>
<body>
<h1>Новый заказ # {{$order->id}} от {{$order->user_name}}</h1>
<br>
<p style="font-weight: 600; font-size: 18px">Личные данные клиента :</p>
<p>ФИО : {{$order->user_name}}</p>
<p>Телефон : {{$order->user_phone}}</p>
<p>Email : {{$order->user_email}}</p>
@if($order->delivery_type == 'delivery')
    <p>Способ доставки : Доставка</p>
    <p>Адрес доставки : {{$order->user_address}}</p>
    <p>Время доставки : {{$order->delivery_date}}</p>
@else
    <p>Способ доставки : Самовывоз</p>
@endif
@if($order->order_comments != null)
    <p>Комментарии клиента : {{$order->order_comments}}</p>
@endif
<p>Сумма заказа : {{number_format($order->amount,0,","," ")}} тг</p>
<table style="margin-bottom: 20px;width: 80%">
    @foreach($products as $product)
        <tr>
            <td style="border: 1px solid #ccc;width: 110px">
                <img style="width: 100px"
                     src="{{ Voyager::image($product->getThumbnail($product->image,'small'))}}"
                     class=""/>
            </td>
            <td style="border: 1px solid #ccc">
                <p style="font-weight: 600;">{{ $product->name }}</p>
                <p>{{ $product->category->name}}</p>
                <p>{!! $product->description !!}</p>
            </td>
            <td style="border: 1px solid #ccc">
                <div>
                    <p style="font-weight: 600;">{{ $product->qty }} шт</p>
                </div>
            <td style="border: 1px solid #ccc">
                <p style="font-weight: 600;">{{number_format($product->product_price,0,","," ")}} тг</p>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>