@extends('app')
@section('title','Страница не найдена')
@section('seo_title', 'Страница не найдена')
@section('meta_keywords','')
@section('meta_description', '')
@section('image','')
@section('url',url()->current())
@section('page_class','page-404')
@section('content')
    <main class="content">
        <section class="content-inner">
            <div class="container">
                <div class="text_block thanks-page">
                    <img src="/img/404.png" class="not-found" alt="Страница не найдена">
                    <h1>Страница не найдена</h1>
                    <p>Такой страницы не существует</p>
                    <div class="btns">
                        <a href="/" class="btn-dark">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- .content -->
@endsection