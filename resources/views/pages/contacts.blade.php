@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','home')
@section('content')
    <div class="breadcrumbs">
        @include('partials.breadcrumbs',['title'=>$page->title])
        <h1 class="titleDark">{{$page->title}}</h1>
    </div>
    <style>
        footer .map, footer .contactUs {
            display: none;
        }
    </style>
    <section id="contact" class="py-3 py-md-5">
        <div class="container">
            <div class="row mb-3 mb-md-5">
                <div class="col-12">
                    <div class="map">
                        {!! $map->value !!}
                    </div>
                </div>
            </div>
            <div class="contactInfo p-4 bg-white">
                <div class="row pb-2">
                    <div class="col-lg-5 mb-2 mb-lg-0">
                        <div class="row align-items-center">
                            <div class="col-sm-auto mb-1 mb-sm-0">
                                <strong>Адрес:</strong>
                            </div>
                            <div class="col-sm-auto mb-2 mb-sm-0">
                                <address class="m-0 text-lg-right">{{$address->value}}</address>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row pb-2">
                    <div class="col-lg-7  mb-2 mb-lg-0">
                        <div class="row align-items-center">
                            <div class="col-sm-auto mb-2 mb-sm-0">
                                <strong>Телефон:</strong>
                            </div>
                            @foreach($contacts as $key=>$contact)
                                <div class="col-sm-auto {{$key == 'astana' ? 'mb-2 mb-sm-0' : ''}}">
                                    <p class="m-0 text-lg-right">{{$key == 'astana' ? 'Нур-Султан' : 'Алматы'}}:&nbsp;<a
                                                href="{{$contact->where('type','phone')->first()->link}}">{{$contact->where('type','phone')->first()->value}}</a>
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row pt-2">
                    <div class="col-12">
                        <div class="row align-items-center">
                            <div class="col-sm-auto mb-1 mb-sm-0">
                                <strong>По всем вопросам пишите нам на:</strong>
                            </div>
                            <div class="col-sm-auto mb-2 mb-sm-0">
                                <p class="m-0 text-lg-right"><a
                                            href="{{$email->link}}">{{ $email->value }}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-5">
                {!! $page->body !!}
            </div>
        </div>
    </section>
@endsection
