@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','home')
@section('content')
    <section id="mainTitle"
             style="background: url('{{Voyager::image($page->getThumbnail($page->image,'big'))}}') no-repeat center fixed;background-size: cover">
        <div class="mainTitle">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-center">{{$page->title}}</h1>
                    </div>
                </div>
                <form class="form-row main-form" action="{{route('feedback')}}" method="post">
                    @csrf
                    <div class="col-md-3">
                        <div class="formStyle">
                            <div class="selectStyle">
                                <select name="eventFormat" id="format">
                                    <option value="Формат">Формат</option>
                                    <option value="Банкет">Банкет</option>
                                    <option value="Кофе-брейк">Кофе-брейк</option>
                                    <option value="Бизнес-ланч">Бизнес-ланч</option>
                                    <option value="Фуршет">Фуршет</option>
                                    <option value="Пикник">Пикник</option>
                                    <option value="Корпоративное питание">Корпоративное питание</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="formStyle">
                            <input type="text" class="form-control" placeholder="Имя *" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="formStyle">
                            <input type="tel" class="form-control" placeholder="Телефон *" name="phone" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="formStyle">
                            <input type="hidden" name="pageLink" value="{{\Request::url()}}">
                            <input type="hidden" class="page-name" name="page"
                                   value="{{isset($page) ? $page->title : ''}}">
                            <input type="hidden" class="page-name" name="is_offer" value="1">
                            <button type="submit" class="btnDark w-100">Получить предложение</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <a href="#about-us" class="nextSection"><img src="img/headerEnd.png" alt=""></a>
    </section>
    <section id="about-us">
        <div class="girl container">
            {!! $page->body !!}
        </div>
        <div class="parallax" data-paroller-factor="-0.5"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 292px; height: 160px; left: 30px; bottom: 50%;background: url('img/about-img-left.png') no-repeat center">
        </div>
        <div class="parallax" data-paroller-factor="0.3"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 253px; height: 292px; right: 30px; bottom: 30%;background: url('img/about-img-right.png') no-repeat center">
        </div>
    </section>
    <section id="set" data-ng-controller="CatalogController as cat">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-auto">
                    <h2 class="titleLight d-block d-md-inline">Готовые сеты для вашего праздника</h2>
                </div>
                <div class="col d-none d-lg-block text-right">
                    <a href="{{route('pages.inner',['menu','sety'])}}" class="float-right">Посмотреть все сеты</a>
                </div>
                <div class="col-auto d-none d-lg-block">
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="setNav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-5" ng-init="cat.initFunctions('sety')">
                <data-owl-carousel class="owl-carousel setCarousel owl-theme w-100 pt-3"
                                   data-options="{nav: !0,navText: ['', ''],navContainer: '.setNav',dots: !1,autoWidth: !0,items: 4,loop: !1,autoplay: !1}">
                    <div owl-carousel-item="" data-ng-repeat="product in cat.products">
                        <div class="set itemProduct">
                            <div class="setImg">
                                <a data-fancybox="gallerySet" href="#" data-ng-href="@{{product.image_link}}"
                                   class="fancy">
                                    <picture>
                                        <source srcset="#" data-ng-srcset="@{{product.webp_link}}"
                                                type="image/webp">
                                        <source srcset="#" data-ng-srcset="@{{product.image_link}}"
                                                type="image/pjpeg">
                                        <img src="#" data-ng-src="@{{product.image_link}}"
                                             alt="@{{product.seo_title ? product.seo_title : product.name}}">
                                    </picture>
                                </a>
                            </div>
                            <div class="setDescription">
                                <div class="setTitle">
                                    <p>@{{product.category.name}}</p>
                                    <h3 class="">&laquo;@{{product.name}}&raquo;</h3>
                                </div>
                                <div class="row text-center setOptions">
                                    <div class="col-4">
                                        <span class="setOpt">@{{product.size}}</span>
                                        <span>персон</span>
                                    </div>
                                    <div class="col-4">
                                        <span class="setOpt">@{{product.price}}</span>
                                        <span>тг</span>
                                    </div>
                                    <div class="col-4">
                                        <span class="setOpt">@{{product.weight}}</span>
                                        <span>грамм</span>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#modalSet@{{ product.id }}"
                               class="leftBtn">Подробнее</a>
                            <a href="javascript:void(0);" class="rightBtn addCartBtn"
                               data-ng-click="cat.addToCart(product,product.qty,$event)"><em class="icn"></em>
                                <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                    <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none"
                                          stroke="#FFFFFF" stroke-width="2" stroke-linecap="square"
                                          stroke-miterlimit="10"
                                          d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </data-owl-carousel>
                <div class="modal fade modal-product"
                     data-ng-repeat="product in cat.products"
                     id="modalSet@{{product.id}}">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L16 16M16 1L1 16" stroke="#AEAEAE" stroke-width="2"
                                          stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </svg>
                            </button>
                            <div class="product-content itemProduct">
                                <picture>
                                    <source srcset="#" data-ng-srcset="@{{product.webp_link}}"
                                            type="image/webp">
                                    <source srcset="#" data-ng-srcset="@{{product.image_link}}"
                                            type="image/pjpeg">
                                    <img src="#" data-ng-src="@{{product.image_link}}" class="img-fluid"
                                         alt="@{{product.seo_title ? product.seo_title : product.name}}">
                                </picture>
                                <div class="content">
                                    <div class="setTitle">
                                        <p>@{{product.category.name}}</p>
                                        <h3 class="">&laquo;@{{product.name}}&raquo;</h3>
                                    </div>
                                    <div class="description" ng-bind-html="product.description"></div>
                                    <div class="text-center setOptions">
                                        <div class="col-7 options">
                                            <div class="col-4">
                                                <span class="setOpt">@{{product.size}}</span>
                                                <span>персон</span>
                                            </div>
                                            <div class="col-4">
                                                <span class="setOpt">@{{product.price}}</span>
                                                <span>тг</span>
                                            </div>
                                            <div class="col-4">
                                                <span class="setOpt">@{{product.weight}}</span>
                                                <span>грамм</span>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0);" class="toCart add-to-cart addCartBtn not-icn"
                                           data-ng-click="cat.addToCart(product,product.qty,$event)"><em>В корзину</em>
                                            <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79"
                                                      fill="none" stroke="#FFFFFF" stroke-width="2"
                                                      stroke-linecap="square" stroke-miterlimit="10"
                                                      d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-5 pt-lg-0">
                <div class="col d-block d-lg-none pr-0">
                    <a href="{{route('pages.inner',['menu','sety'])}}" class="float-left">Посмотреть все сеты</a>
                </div>
                <div class="col-auto d-block d-lg-none text-right pl-0 py-2">
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="setNav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="service">
        <div class="container">
            <h2 class="titleDark d-block d-md-inline">Услуги, предлагаемые нашей кейтеринговой компанией</h2>
            <div class="row pt-4">
                @foreach($services->chunk(2) as $s=>$chunk)
                    <div class="row">
                        @if($s % 2 == 0)
                            @foreach($chunk as $k=>$service)
                                <div class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-7 col-sm-6 @else col-lg-5 col-sm-6 @endif @else col-12 @endif  serviceItem">
                                    <div class="service row"
                                         style="background: url('{{Voyager::image($service->getThumbnail($service->image,'big'))}}') no-repeat left bottom;background-size: cover">
                                        <h3 class="col-12 align-self-center"><img
                                                    src="{{Voyager::image($service->icon)}}"
                                                    alt="{{$service->seo_title}}">{{$service->title}}
                                        </h3>
                                        <p class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-7 @else col-lg-10 @endif @else col-lg-7 @endif">{{$service->excerpt}}</p>
                                        <p class="col-12 d-lg-block"><a
                                                    href="{{route('pages.inner',['uslugi',$service])}}">Подробнее</a>
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            @foreach($chunk as $k=>$service)
                                <div class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-5 col-sm-6 @else col-lg-7 col-sm-6 @endif @else col-12 @endif  serviceItem">
                                    <div class="service row"
                                         style="background: url('{{Voyager::image($service->getThumbnail($service->image,'big'))}}') no-repeat left bottom;background-size: cover">
                                        <h3 class="col-12 align-self-center"><img
                                                    src="{{Voyager::image($service->icon)}}"
                                                    alt="{{$service->seo_title}}">{{$service->title}}
                                        </h3>
                                        <p class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-10 @else col-lg-7 @endif @else col-lg-7 @endif">{{$service->excerpt}}</p>
                                        <p class="col-12 d-lg-block"><a
                                                    href="{{route('pages.inner',['uslugi',$service])}}">Подробнее</a>
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

        <div class="parallax" data-paroller-factor="0.3"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 200px; z-index:-1;height: 200px; left: 30px; top: 50%;background: url('img/stl.png') no-repeat center">
        </div>
        <div class="parallax" data-paroller-factor="-0.5"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 144px; height: 163px; left: 30px; bottom: 50%;background: url('img/sbl.png') no-repeat center">
        </div>
        <div class="parallax" data-paroller-factor="-0.5"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 257px; height: 265px; right: 30px; top: 0%;background: url('img/str.png') no-repeat center">
        </div>
        <div class="parallax" data-paroller-factor="0.3"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 294px; height: 309px; right: 30px; bottom: 0%;background: url('img/sbr.png') no-repeat center">
        </div>
    </section>
    <section id="privilege">
        <div class="container">
            <div class="row">
                @foreach($advantages as $advantage)
                    <div class="item col-lg-2 col-md-4 col-6 py-2 text-center align-self-start">
                        <div class="privImg"><img src="{{Voyager::image($advantage->icon)}}"
                                                  alt="{{$advantage->title}}"/></div>
                        <p>{{$advantage->title}}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section id="photoGallery">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-auto">
                    <h2 class="titleDark d-block d-md-inline">Наша фотогалерея</h2>
                </div>
                <div class="col d-none d-lg-block text-right">
                    <a href="{{route('pages.show','gallery')}}" class="float-right">Все фотографии</a>
                </div>
                <div class="col-auto d-none d-lg-block">
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="photoNav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="container photoContainer px-lg-0">
            <div class="row">
                <div class="owl-carousel photoCarousel owl-theme">
                    @foreach(json_decode($album->gallery) as $photo)
                        @if(\App\Album::mimeType(public_path('storage/').$photo))
                            <div class="photo">
                                <picture>
                                    <source srcset="{{str_replace('.'.pathinfo(\Voyager::image($photo),PATHINFO_EXTENSION),'.webp',\Voyager::image($photo))}}"
                                            type="image/webp">
                                    <source srcset="{{\Voyager::image($photo)}}" type="image/pjpeg">
                                    <img src="{{\Voyager::image($photo)}}" alt="{{$album->seo_title}}">
                                </picture>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="row pt-5 pt-lg-0">
                <div class="col d-block d-lg-none">
                    <a href="{{route('pages.show','gallery')}}">Все фотографии</a>
                </div>
                <div class="col-auto d-block d-lg-none text-right py-2">
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="photoNav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
    </section>
    <section id="callToAction">
        <div class="container">
            <h2 class="titleLight text-center text-sm-left d-block d-md-inline">Хотите, чтобы ваше мероприятие прошло
                идеально? </h2>
            <div class="form-row">
                <div class="col-md-3">
                    <div class="formStyle">
                        <div class="selectStyle">
                            <select name="formatEvent" id="formatEvent">
                                <option value="Формат">Формат</option>
                                <option value="Банкет">Банкет</option>
                                <option value="Кофе-брейк">Кофе-брейк</option>
                                <option value="Бизнес-ланч">Бизнес-ланч</option>
                                <option value="Фуршет">Фуршет</option>
                                <option value="Пикник">Пикник</option>
                                <option value="Корпоративное питание">Корпоративное питание</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="formStyle">
                        <input type="number" name="qtyPerson" placeholder="Количество персон">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="formStyle">
                        <input type="text" name="dateEvent" placeholder="Дата события" class="datepickerHere">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="formStyle">
                        <a href="#" data-toggle="modal" data-target="#modalOffer">Получить предложение</a>
                    </div>
                </div>
            </div>
            <div class="row d-none d-lg-flex text-white">
                <div class="col-4 pr-0 pl-5 align-self-center text-right"><i class="ctaArrow"></i>Индивидуальный подход
                </div>
                <div class="col-4 pr-0 pl-5 align-self-center text-center"><i class="ctaArrow"></i>Огромный выбор блюд
                </div>
                <div class="col-4 pr-0 pl-5 align-self-center"><i class="ctaArrow"></i>Гарантия лучшей цены</div>
            </div>
        </div>
    </section>
    <section id="comments">
        <div class="container">
            <h2 class="titleDark text-center text-sm-left d-block d-md-inline">98% клиентов довольны результатом</h2>
            <h3>Обращаясь в Astana Catering, Вы всегда получите профессиональный подход и высокое качество проведения
                мероприятий.</h3>
            <div class="row text-center">
                @foreach($reviews as $k=>$review)
                    <div class="col-lg-4 @if($k == 0) col-md-6 @endif @if($k == 1) col-md-6 d-none d-md-block @endif @if ($k == 2) d-none d-lg-block @endif">
                        <div class="commentsImg">
                            <picture>
                                <source srcset="{{$review->webpImage}}" type="image/webp">
                                <source srcset="{{$review->bigThumb}}" type="image/pjpeg">
                                <img src="{{$review->bigThumb}}" alt="{{$review->name}}"
                                     class="img-fluid">
                            </picture>
                        </div>
                        <h4>{{$review->name}}</h4>
                        <h5>{{$review->company}}</h5>
                        <p>{{Str::limit($review->content,275,'...')}}</p>
                        <a href="#" data-toggle="modal" data-target="#modalReview{{$review->id}}" class="readNext">Читать
                            отзыв полностью</a>
                        <div class="modal fade modal-product" id="modalReview{{$review->id}}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 1L16 16M16 1L1 16" stroke="#AEAEAE" stroke-width="2"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round"/>
                                        </svg>
                                    </button>
                                    <div class="review-content">
                                        <picture>
                                            <source srcset="{{$review->webpImage}}" type="image/webp">
                                            <source srcset="{{$review->bigThumb}}" type="image/pjpeg">
                                            <img src="{{$review->bigThumb}}" alt="{{$review->name}}"
                                                 class="img-fluid">
                                        </picture>
                                        <div class="content">
                                            <div class="setTitle">
                                                <h4 class="title">{{$review->name}}</h4>
                                                <p class="company">{{$review->company}}</p>
                                            </div>
                                            <div class="description">{{$review->content}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row pt-5">
                <div class="col-md-6 text-center text-md-right py-2">
                    <a href="{{route('pages.show',['otzyvy'])}}" class="btnDark">Все отзывы</a>
                </div>
                <div class="col-md-6 text-center text-md-left py-2">
                    <a href="#" data-target="#modalNewReview" data-toggle="modal" class="btnLight">Добавить отзыв</a>
                </div>
            </div>
        </div>

        <div class="parallax" data-paroller-factor="0.3"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 356px; height: 183px; right: 30px; bottom: 30%;background: url('img/cbr.png') no-repeat center">
        </div>
    </section>
    <section id="partners">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-auto">
                    <h2 class="titleDark d-block d-md-inline">Наши партнеры</h2>
                </div>
                <div class="col d-none d-lg-block">
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="partNav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="owl-carousel partCarousel">
                    @foreach($partners as $partner)
                        <div class="partItem">
                            <picture>
                                <source srcset="{{$partner->webpImage}}" type="image/webp">
                                <source srcset="{{$partner->bigThumb}}" type="image/pjpeg">
                                <img src="{{$partner->bigThumb}}" alt="{{$partner->name}}"
                                     class="img-fluid">
                            </picture>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section id="news">
        <div class="container">
            <div style="display: flex;align-items: center;">
                <div style="padding-right: 80px;">
                    <h2 class="titleDark d-block d-md-inline">Новости и статьи</h2>
                </div>
                <div>
                    <a href="{{route('pages.show','novosti')}}" class="btnDark">Все статьи</a>
                </div>
            </div>
            <div class="row allNews">
                @foreach($posts as $post)
                    <div class="col-sm-6 news">
                        <div class="row m-0 p-0">
                            <div class="col-lg-6 p-0 m-0">
                                <div class="newsText">
                                    <h3><a href="{{route('pages.inner',['novosti',$post])}}">{{$post->title}}</a></h3>
                                    <p>{{$post->excerpt}}</p>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0 m-0">
                                <div class="newsImg">
                                    <picture>
                                        <source srcset="{{$post->webpImage}}" type="image/webp">
                                        <source srcset="{{$post->bigThumb}}" type="image/pjpeg">
                                        <img src="{{$post->bigThumb}}" alt="{{$post->seo_title}}"
                                             class="img-fluid">
                                    </picture>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12 text-center pt-4">
                    <a href="#modalCallback" data-toggle="modal" class="btnLight" style="padding: 30px;background: #853334;border-radius: 50px;min-width: 300px;">Оставить заявку</a>
                </div>
            </div>
        </div>

        <div class="parallax" data-paroller-factor="0.5"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 267px; height: 275px; left: 30px; bottom: 40%;background: url('img/nbl.png') no-repeat center">
        </div>
        <div class="parallax" data-paroller-factor="-0.3"
             data-paroller-type="foreground"
             data-paroller-direction="vertical"
             style="width: 265px; height: 309px; right: 30px; top: 30%;background: url('img/ntr.png') no-repeat center">
        </div>
    </section>
@endsection

@section('scripts')
    <script src="/js/angular/catalog.controller.js"></script>
@endsection