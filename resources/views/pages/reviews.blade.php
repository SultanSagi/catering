@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','reviews-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title])
    <h1 class="titleDark">{{$page->title}}</h1>
</div>
<section id="comments">
    <div class="container">
        <div class="row text-center">
            @foreach($reviews as $k=>$review)
                <div class="col-lg-4 @if($k == 0) col-md-6 @endif @if($k == 1) col-md-6 d-none d-md-block @endif @if ($k == 2) d-none d-lg-block @endif">
                    <div class="commentsImg">
                        <picture>
                            <source srcset="{{$review->webpImage}}" type="image/webp">
                            <source srcset="{{$review->bigThumb}}" type="image/pjpeg">
                            <img src="{{$review->bigThumb}}" alt="{{$review->name}}"
                                 class="img-fluid">
                        </picture>
                    </div>
                    <h4>{{$review->name}}</h4>
                    <h5>{{$review->company}}</h5>
                    <p>{{Str::limit($review->content,275,'...')}}</p>
                    <a href="#" data-toggle="modal" data-target="#modalReview{{$review->id}}" class="readNext">Читать
                        отзыв полностью</a>
                    <div class="modal fade modal-product" id="modalReview{{$review->id}}">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 1L16 16M16 1L1 16" stroke="#AEAEAE" stroke-width="2"
                                              stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                    </svg>
                                </button>
                                <div class="review-content">
                                    <picture>
                                        <source srcset="{{$review->webpImage}}" type="image/webp">
                                        <source srcset="{{$review->bigThumb}}" type="image/pjpeg">
                                        <img src="{{$review->bigThumb}}" alt="{{$review->name}}"
                                             class="img-fluid">
                                    </picture>
                                    <div class="content">
                                        <div class="setTitle">
                                            <h4 class="title">{{$review->name}}</h4>
                                            <p class="company">{{$review->company}}</p>
                                        </div>
                                        <div class="description">{{$review->content}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row pt-5">
            <div class="col-md-12 text-center text-md-center py-2">
                <a href="#" data-target="#modalNewReview" data-toggle="modal" class="btnLight">Добавить отзыв</a>
            </div>
        </div>
    </div>

    <div class="parallax" data-paroller-factor="0.3"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 356px; height: 183px; right: 30px; bottom: 30%;background: url('img/cbr.png') no-repeat center">
    </div>
</section>
@endsection