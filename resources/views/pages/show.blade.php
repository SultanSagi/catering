@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','home')
@section('content')
    <div class="breadcrumbs">
        @include('partials.breadcrumbs',['title'=>$page->title])
        <h1 class="titleDark">{{$page->title}}</h1>
    </div>
    <section id="aboutUs">
        <div class="container">
            {!! $page->body !!}
            @if(count(json_decode($page->files)))
                <div class="row d-flex align-items-center">
                    @foreach (json_decode($page->files) as $file)
                        @php
                            $name = explode('/',$file)[array_key_last(explode('/',$file))];
                        @endphp
                        <div class="col-lg-4 col-md-4 col-sm-6 p-3">
                            <div class="galleryProductItem {{App\Page::mimeType($file)}}">
                                <a href="{{Voyager::image($file)}}" target="_blank">{{$name}}</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </section>
@endsection