@extends('app')
@section('title','Спасибо за заявку')
@section('seo_title', 'Спасибо за заявку')
@section('meta_keywords','Спасибо за заявку')
@section('meta_description', 'Спасибо за заявку')
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','page-other')
@section('content')
    <main class="content">
        <section class="content-inner">
            <div class="container">
                <div class="text_block thanks-page">
                    <img src="/img/success.png" alt="Спасибо за заявку">
                    <h1>Спасибо за заявку, {{$_REQUEST['name']}}</h1>
                    <p>Мы свяжемся с вами в ближайшее время</p>
                    <div class="btns">
                        <a href="/" class="btn-white">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- .content -->
@endsection