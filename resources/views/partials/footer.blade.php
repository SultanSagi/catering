@if(!in_array(\Request::url(),[route('pages.show',['cart']),route('pages.show',['checkout'])]))
    <a id="shopCart" href="{{route('pages.show',['cart'])}}" data-ng-controller="CartController as cart"
       class="shopCart hide">
        <img src="/img/shoppingBasket.png" class="" alt="корзина" data-ng-init="cart.getCartContent()">
        <span class="count" data-ng-bind="cart.cartItemsCount"></span>
    </a>
@endif

<div class="container" style="margin-bottom:15px;text-align:center;color:#fff;">
    <div class="row align-items-center round-elem-pr">
        <div class="round-elem col-12 col-lg-4" style="padding:15px;background:#853334;border-radius:50px;">
            Заявки принимаются за 24 часа до времени доставки.
        </div>
        <div class="round-elem col-12 col-lg-4" style="padding:15px;background:#853334;border-radius:50px;">
            Время приема заявок<br>с 9 до 21 ежедневно.
        </div>
        <div class="round-elem col-12 col-lg-4" style="padding:15px;background:#853334;border-radius:50px;">
            Доставка от 20000тг<br>БЕСПЛАТНО.
        </div>
    </div>
</div>

<footer>
    <div class="map">
        {!! $map->value !!}
        <a href="#top" class="scrollToTop"><img src="/img/top.png" alt="scrollTotop"></a>
    </div>
    <div class="contactUs">
        <h2>Контакты</h2>
        <address>{{$address->value}}</address>
        <a href="{{$phones->first()->link}}">{{$phones->first()->value}}</a>
        <a href="{{$email->link}}">{{$email->value}}</a>
    </div>
    <div class="bottomLine">
        <div class="container">
            <div class="row py-4 ">
                <div class="col-sm-6 text-center text-sm-left">{{setting('site.copyrights')}}</div>
                <div class="col-sm-6 text-center text-sm-right">Разработано компанией <a class="text-indent"
                                                                                         href="https://i-marketing.kz"
                                                                                         rel="nofollow">«IMarketing»</a>
                </div>
            </div>
        </div>
    </div>
</footer>