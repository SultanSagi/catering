<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{--<meta name="theme-color" content="#3f0e0b"/>--}}
    {{--<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">--}}
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <meta name="title" content="@yield('seo_title')"/>
	<link rel="shortcut icon" href="/fav.png" type="image/png">
    <title>@if(strlen($__env->yieldContent('seo_title')) > 2) @yield('seo_title') @else @yield('title') @endif | Astana Catering</title>
    <link rel="canonical" href="{{url()->current()}}">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta property="og:title" content="@if(strlen($__env->yieldContent('seo_title')) > 2) @yield('seo_title') @else @yield('title') @endif | Astana Catering"/>
    <meta property="og:description" content="@yield('meta_description')"/>
    <meta property="og:url" content={{url()->current()}}/>
    <meta property="og:image" content="@if(strlen($__env->yieldContent('image')) > 2) @yield('image') @else {{ env('APP_URL').'/images/og.jpg'}} @endif"/>
    <meta property="og:image:type" content="image/jpeg"/>
    <meta property="og:image:width" content="300"/>
    <meta property="og:image:height" content="300"/>
	<meta name="yandex-verification" content="c9851fc8f43feed8" />
	<meta name="google-site-verification" content="dfWa8C4gQVToGdwrWXz4dg1UmTkuKoJYnQRoTp4nwjw" />


    <link href="/css/main.min.css?v=300120202" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js?v=23122019"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js?v=23122019"></script>
    <![endif]-->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N8V9HQL');</script>
	<!-- End Google Tag Manager -->
</head>