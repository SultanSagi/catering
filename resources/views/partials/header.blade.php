<div class="topLine"  data-ng-init="hc.init()">
    <div class="container">
        <div class="row d-flex d-lg-none align-items-center">
            <div class="col-5 col-sm-4">
                <span>{{$graph->value}}</span>
            </div>
            <div class="col-7 col-sm-8 text-right">
                <ul class="list-inline m-0">
                    @foreach($phones as $phone)
                        <li class="list-inline-item m-0 @if($phone->city == 'astana') mr-md-5 @else d-none d-md-inline @endif">
                            {{$phone->city == 'astana' ? 'Нур-Султан' : 'Алматы'}}:&nbsp;&nbsp;<a
                                    href="{{ $phone->link}}">{{ $phone->value }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row align-items-center justify-content-center">
            <div class="topLineLogo col py-3">
                <a class="" href="/">
                    <img src="{{Voyager::image(setting('site.logo'))}}" alt="{{setting('site.title')}}"
                         class="img-fluid">
                </a>
            </div>
            @foreach($contacts as $key=>$contact)
                <div class="topLineCity {{$key}} col d-none d-lg-block py-3">
                    <div class="row d-lg-block d-xl-flex">
                        <div class="col-auto pr-0 align-self-center pl-0">{{$key == 'astana' ? 'Нур-Султан' : 'Алматы'}}:</div>
                        <div class="col-auto">
                            <div class="row">
                                <div class="col-auto"><a
                                            href="{{$contact->where('type','phone')->first()->link}}">{{$contact->where('type','phone')->first()->value}}</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-auto">{{$contact->where('type','graph')->first()->value}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="topLineBtn col-auto text-right text-xl-left py-3">
                <ul class="list-inline mb-0">
                    <li class="topLineBtnSearch list-inline-item">
                        <a href="#" data-toggle="modal" data-target="#searchForm"><img src="/img/searchIcon.png"
                                                                                       alt="поиск по сайту"></a>
                    </li>
                    <li class="topLineBtnBasket list-inline-item">
                        <a href="{{route('cart.index')}}">
                            <img src="/img/shoppingBasket.png" class="shopping-cart" alt="корзина">
                            <span data-ng-bind="hc.cartItems"></span>
                        </a>
                    </li>
                    <li class="topLineBtnCallback d-none d-md-inline list-inline-item">
                        <a href="#">Заказать звонок</a>
                    </li>
                    <li class="topLineBtnMenu d-inline d-md-none list-inline-item">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarContent">
                            <img src="/img/btnMenu.png" alt="Мобильная кнопка меню">
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<header id="top" data-ng-controller="HeaderController as hc">
    {{menu('menu','partials.menu')}}
</header>
