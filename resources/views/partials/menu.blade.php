<nav class="navbar navbar-expand-md">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav row w-100 text-right text-md-left ml-0">

                @php

                    if (Voyager::translatable($items)) {
                        $items = $items->load('translations');
                    }

                @endphp

                @foreach ($items as $item)

                    @php

                        $originalItem = $item;
                        if (Voyager::translatable($item)) {
                            $item = $item->translate($options->locale);
                        }

                        $isActive = null;
                        $styles = null;
                        $icon = null;

                        // Background Color or Color
                        if (isset($options->color) && $options->color == true) {
                            $styles = 'color:'.$item->color;
                        }
                        if (isset($options->background) && $options->background == true) {
                            $styles = 'background-color:'.$item->color;
                        }

                        // Check if link is current
                        if(url($item->link()) == url()->current()){
                            $isActive = 'active';
                        }

                        // Set Icon
                        if(isset($options->icon) && $options->icon == true){
                            $icon = '<i class="' . $item->icon_class . '"></i>';
                        }
                    @endphp

                    <li class="nav-item  {{(($item->title == trim($item->title) && strpos($item->title, ' ') !== false) && ($item->link() != '/o-nas' )) ? 'col-auto': 'col-md'}} justify-content-center text-center d-inline-flex align-items-center {{ $isActive }}">
                        <a href="{{ url($item->link()) }}" target="{{ $item->target }}" class="nav-link {{$isActive}}"
                           style="{{ $styles }}">{!! $icon !!}{{ $item->title }}</a>
                        @if(!$originalItem->children->isEmpty())
                            @include('voyager::menu.default', ['items' => $originalItem->children, 'options' => $options])
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>
