<div class="modal fade" id="searchForm">
    <div class="modal-dialog" role="document">
        <div class="modal-content" data-ng-controller="SearchController as sc">
            <div class="modal-header">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col">

                            <form method="GET" id="searchHeader">
                                <input type="search" name="q" placeholder="Найти ..." class="searchHeader w-100" data-ng-model="searchInput" autocomplete="off">
                                <button class="d-none d-sm-inline searchBtn" data-ng-click="sc.searchByInput(searchInput)">Найти</button>
                                {{-- <button type="submit">Search</button> --}}
                            </form>
                        </div>
                        <div class="col-auto text-right">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="row pt-4 p-sm-0">
                        <div class="col text-center">
                            <button type="button" class="d-inline d-sm-none searchBtn" data-ng-click="sc.searchByInput(searchInput)">Найти</button>
                        </div>
                    </div>
                    <div class="items"><a data-ng-repeat="item in sc.searchItems track by $index" class="item"
                                          data-ng-href="@{{item.full_link}}">@{{item.item}}:
                            <span>@{{item.name}}</span> </a>
                        <p data-ng-if="sc.searchItems.length < 1">По вашему запросу ничего не найдено.</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-form" id="modalCallback">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-simple">
            <form action="{{route('feedback')}}" method="post">
                @csrf
                <h5 class="modal-title text-left" id="exampleModalLabel">Оставить заявку</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <input type="text" class="form-control" placeholder="Имя *" name="name" required>
                <input type="tel" class="form-control" placeholder="Телефон *" name="phone" required>
                <input type="email" class="form-control" placeholder="E-mail" name="email">
                <input type="hidden" name="pageLink" value="{{\Request::url()}}">
                <input type="hidden" class="page-name" name="page" value="{{isset($page) ? $page->title : ''}}">
                <input type="hidden" class="page-name" name="is_offer" value="0">
                <button type="submit" class="btnDark w-100" id="Mohammed">Отправить заявку</button>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modal-form" id="modalOffer">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-simple">
            <form action="{{route('feedback')}}" method="post">
                @csrf
                <h5 class="modal-title text-left" id="exampleModalLabel2">Получить предложение</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="formStyle p-0">
                    <div class="selectStyle">
                        <select name="eventFormat" id="eventFormat">
                            <option value="Формат">Формат</option>
                            <option value="Банкет">Банкет</option>
                            <option value="Кофе-брейк">Кофе-брейк</option>
                            <option value="Бизнес-ланч">Бизнес-ланч</option>
                            <option value="Фуршет">Фуршет</option>
                            <option value="Пикник">Пикник</option>
                            <option value="Корпоративное питание">Корпоративное питание</option>
                        </select>
                    </div>
                </div>
                <div class="formStyle">
                    <input type="number" name="count" placeholder="Количество персон">
                </div>
                <div class="formStyle">
                    <input type="text" name="date" class="datepickerHere" placeholder="Дата события" autocomplete="off">
                </div>
                <input type="text" class="form-control" placeholder="Имя *" name="name" required>
                <input type="tel" class="form-control" placeholder="Телефон *" name="phone" required>
                <input type="email" class="form-control" placeholder="E-mail" name="email">
                <input type="hidden" name="pageLink" value="{{\Request::url()}}">
                <input type="hidden" class="page-name" name="page" value="{{isset($page) ? $page->title : ''}}">
                <input type="hidden" class="page-name" name="is_offer" value="1">
                <button type="submit" class="btnDark w-100" id="Julianne">Отправить заявку</button>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modal-form" id="modalNewReview">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('reviews.add')}}" method="post" enctype="multipart/form-data">
                @csrf
                <h5 class="modal-title">Оставить отзыв</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <input type="text" name="name" placeholder="Имя *" required>
                <input type="email" name="email" placeholder="E-mail">
                <input type="text" name="company" placeholder="Компания">
                <textarea name="content" cols="30" rows="7" placeholder="Ваш отзыв" required></textarea>
                <div class='file-input'>
                    <input type='file' name="photo">
                    <span class='button'>Прикрепить фото</span>
                    <span class='label' data-js-label>Вы можете прикрепить фото не больше 15 мб.</span>
                </div>
                <button type="submit" class="btnDark w-100">Отправить отзыв</button>
            </form>
        </div>
    </div>
</div>
<div class="modal  modal-form" id="successModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <img src="/img/success.png" alt="Успешно">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1L16 16M16 1L1 16" stroke="#AEAEAE" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round"/>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <h3>Спасибо</h3>
                <p>Ваш отзыв успешно отправлен</p>
                <a href="#" class="btnDark w-100" data-dismiss="modal" aria-label="Close">Закрыть</a>
            </div>
        </div>
    </div>
</div>