<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="/css/angularjs-slider.min.css?v=300120202" rel="stylesheet">
<link href="/css/change.css?v=300120202" rel="stylesheet">
<link href="/css/datepicker.min.css?v=300120202" rel="stylesheet">
<link href="/css/wickedpicker.min.css?v=300120202" rel="stylesheet">

<script src="/js/test.js?v=300120202"></script>
<script src="/js/angular/angular.min.js?v=300120202"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js?v=300120202"></script>
<script src="/js/jquery.ui.touch-punch.js?v=300120202"></script>
<script src="/js/jquery.maskedinput.js?v=300120202"></script>
<script src="/js/ui-bootstrap.js?v=300120202"></script>
<script src="/js/angular/ng-sanitize.js?v=300120202"></script>
<script src="/js/angular/angularjs-slider.min.js?v=300120202"></script>
<script src="/js/wickedpicker.min.js?v=300120202"></script>
<script src="/js/datepicker.min.js?v=300120202"></script>
<script src="/js/angular/app.module.js?v=300120202"></script>
<script src="/js/angular/api.service.js?v=300120202"></script>
<script src="/js/angular/header.controller.js?v=300120202"></script>
<script src="/js/angular/cart.controller.js?v=300120202"></script>
@yield('scripts')
<script src="/js/change.js?v=300120202"></script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N8V9HQL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
