@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','posts-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title])
    <h1 class="titleDark">{{$page->title}}</h1>
</div>
<section id="news">
    <div class="container">
        <div class="row allNews">
            @foreach ($posts as $post)
                <div class="col-sm-6 news">
                    <div class="row m-0 p-0">
                        <div class="col-lg-6 p-0 m-0">
                            <div class="newsText">
                                <h3><a href="{{route('pages.inner',[$page,$post])}}">{{$post->title}}</a></h3>
                                <p>{{$post->excerpt}}</p>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0 m-0">
                            <div class="newsImg">
                                <picture>
                                    <source srcset="{{$post->webpImage}}" type="image/webp">
                                    <source srcset="{{$post->bigThumb}}" type="image/pjpeg">
                                    <img src="{{$post->bigThumb}}" alt="{{$post->seo_title}}"
                                         class="img-fluid">
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="parallax" data-paroller-factor="0.5"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 267px; height: 275px; left: 30px; bottom: 40%;background: url('{{ asset('img/nbl.png') }}') no-repeat center">
    </div>
    <div class="parallax" data-paroller-factor="-0.3"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 265px; height: 309px; right: 30px; top: 30%;background: url('{{ asset('img/ntr.png') }}') no-repeat center">
    </div>
</section>
@endsection