@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $post->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $post->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$post->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $post->meta_description))
@section('image',$post->thumbic)
@section('url',url()->current())
@section('page_class','service-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title,'titleLink'=>route('pages.show',$page),'subtitle'=>$post->title])
    <h1 class="titleDark">{{$post->title}}</h1>
</div>
<section id="aboutUs">
    <div class="container">
        {!! $post->body !!}
    </div>
</section>
@endsection