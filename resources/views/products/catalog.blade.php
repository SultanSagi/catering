@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','albums-page')
@section('content')
    <div class="breadcrumbs">
        @include('partials.breadcrumbs',['title'=>$page->title])
        <h1 class="titleDark">{{$page->title}}</h1>
    </div>
    <section id="menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
                        @foreach($categories as $category)
                            <div class="items col-lg-3 col-sm-6 px-2 grid-group-item">
                                <div class="menuItem">
                                    <div class="menuItemImg">
                                        <a href="{{route('pages.inner',[$page,$category->slug])}}">
                                            <picture>
                                                <source srcset="{{str_replace('.'.pathinfo(Voyager::image($category->image),PATHINFO_EXTENSION),'-small.webp',Voyager::image($category->image))}}"
                                                        type="image/webp">
                                                <source srcset="{{Voyager::image($category->getThumbnail($category->image,'small'))}}"
                                                        type="image/pjpeg">
                                                <img src="{{Voyager::image($category->getThumbnail($category->image,'small'))}}"
                                                     alt="{{$category->seo_title}}">
                                            </picture>
                                        </a>
                                    </div>
                                    <div class="menuItemDesc categoryBtns">
                                        <h3><a href="">{{ $category->name }}</a></h3>
                                        <p>{{$category->description}}</p>
                                        <a href="{{route('pages.inner',[$page,$category->slug])}}" class="toCart">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-12 pt-3">

                        <!--
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span class="arrows" aria-hidden="true">&#10229;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span class="arrows" aria-hidden="true">&#10230;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
