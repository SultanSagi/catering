@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $category->name))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $category->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$category->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $category->meta_description))
@section('image',$category->thumbic)
@section('url',url()->current())
@section('page_class','products-page')
@section('content')
    <div class="breadcrumbs">
        @include('partials.breadcrumbs',['title'=>$page->title, 'titleLink'=>route('pages.show',$page),'subtitle'=>$category->name])
        <h1 class="titleDark">{{$category->name}}</h1>
    </div>
    <section id="menu" ng-controller="CatalogController as cat">
        <div class="container" ng-init="cat.initFunctions('{{$category->slug}}')">
            <div class="row">
                <div class="col-12 d-block d-md-none text-center">
                    <button class="btnFilter border-0" data-toggle="modal" data-target="#filterModal" data-ng-click="cat.refreshSlider()">Фильтр товаров
                    </button>
                </div>
                <div class="col-lg-3 col-md-4 pr-4" ng-show="!cat.filters.isEmpty">
                    <div class="btnView float-left d-none d-md-block" ng-show="cat.currentCategory.id !=3">
                        <button class="btnViewList"></button>
                        <button class="btnViewPlit active"></button>
                    </div>
                    <div class="btnCountView float-right d-none d-md-block"  ng-show="cat.currentCategory.id !=3">
                        <ul class="list-unstyled">
                            <li class="init">Кол-во: <span>@{{ cat.pageSize }}</span></li>
                            <li data-ng-class="{'active' : cat.pageSize==12}" data-ng-click="cat.changePageSize(12)">12</li>
                            <li data-ng-class="{'active' : cat.pageSize==24}" data-ng-click="cat.changePageSize(24)">24</li>
                            <li data-ng-class="{'active' : cat.pageSize==36}" data-ng-click="cat.changePageSize(36)">36</li>
                            <li data-ng-class="{'active' : cat.pageSize==48}" data-ng-click="cat.changePageSize(48)">48</li>
                        </ul>
                    </div>
                    <div class="" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog mt-0" role="document">
                            <div class="modal-content border-0">
                                <div class="modal-body p-0 border-0">
                                    <div class="filter w-100" ng-class="cat.currentCategory.id ==3 ? 'pt-0' : ''">
                                        <div id="accordion">
                                            <div class="filter-item ui-accordion-header-collapsed" data-ng-repeat="(categoryKey, category) in cat.filters"
                                                 data-ng-init="cat.filter[category]={}" data-ng-if="categoryKey !='price'">
                                                <h3 class="accordion-title" data-ng-if="categoryKey =='weight'">Вес</h3>
                                                <h3 class="accordion-title" data-ng-if="categoryKey =='filterCategory'">Вид</h3>
                                                <h3 class="accordion-title" data-ng-if="categoryKey == 'tag'">Тип</h3>
                                                <h3 class="accordion-title" data-ng-if="categoryKey=='size'">Количество человек</h3>
                                                <div class="qtyAccordion">
                                                    <div class="w-100" data-ng-repeat="(key, value) in category | filtersLimitTo: category.filtersLimit"
                                                         data-ng-if="key !='filtersLimit'">
                                                    <label class="cont">@{{key}} @{{ (categoryKey == 'weight') ? 'гр':'' }}
                                                        <input type="checkbox"  data-ng-model="cat.filters[categoryKey][key]"
                                                               name="filter">
                                                        <!-- <button>test</button> -->
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    </div>
                                                    <a class="more more-filters" data-ng-show="cat.showMoreFilter(category) && Object.keys(category).length > 5"
                                                       data-ng-click="category.filtersLimit=category.length">Показать больше</a>
                                                    <a class="more more-filters" data-ng-show="!cat.showMoreFilter(category) && Object.keys(category).length > 5"
                                                       data-ng-click="category.filtersLimit=4">Скрыть</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="priceFilter">
                                            <h3>Цена</h3>

                                            <div class="priceContent">
                                                <div data-rzslider data-rz-slider-model="cat.filters.price.min"
                                                     data-rz-slider-high="cat.filters.price.max"
                                                     data-rz-slider-options="cat.filters.price.options"></div>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <input class="form-control" data-ng-model="cat.filters.price.min">
                                                    </div>
                                                    <div class="col-auto">&#9473;</div>
                                                    <div class="col">
                                                        <input class="form-control" data-ng-model="cat.filters.price.max">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="anyQuest">
                                            <h3>Остались вопросы?</h3>
                                            <a href="{{$phones->first()->link}}">{{$phones->first()->value}}</a>
                                            <p>{{$graph->value}}</p>
                                            <small>Затрудняетесь с выбором? Поможем с подбором меню!</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer d-md-none">
                                    <button type="button" class="btnDark d-block w-100" data-dismiss="modal">Закрыть
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="row" data-ng-if="cat.products.length">
                        <div class="items product-item col-lg-4 col-sm-6 px-2 grid-group-item"
                             data-ng-repeat="product in cat.filtered_products=(cat.products | filter:cat.updateFilters) | limitTo:cat.pageSize:(cat.currentPage - 1) * cat.pageSize track by product.id">
                            <div class="menuItem itemProduct" ng-if="cat.currentCategory.id != 3">
                                <div class="menuItemImg">
                                    <a data-fancybox="galleryMenu" data-ng-href="@{{product.image_link}}" href="#">
                                        <picture>
                                            <source srcset="#" data-ng-srcset="@{{product.webp_link}}"
                                                    type="image/webp">
                                            <source srcset="#" data-ng-srcset="@{{product.image_link}}"
                                                    type="image/pjpeg">
                                            <img src="#" data-ng-src="@{{product.image_link}}"
                                                 alt="@{{product.seo_title ? product.seo_title : product.name}}">
                                        </picture>
                                    </a>
                                </div>
                                <div class="menuItemDesc">
                                    <h3>@{{product.name}}</h3>
                                    <div ng-bind-html="product.description"></div>
                                </div>
                                <div class="menuItemBtns">
                                    <div class="row">
                                        <div class="col-auto pr-0 mr-3 price">
                                            <input type="button" value="-" class="minusBtn" data-ng-click="cat.minus(product)">
                                            <input type="number" name="cnt" placeholder="1" value="" data-ng-change="cat.checkForMin(product)" data-ng-blur="cat.checkForMin(product)" data-ng-model="product.qty"  min="@{{ product.min_value }}" class="qtyInput">
                                            <input type="button" value="+" class="plusBtn" data-ng-click="cat.plus(product)">
                                        </div>
                                        <div class="col pl-0 price">
                                            <span class="price">@{{ product.price }} @{{ product.price_value }}</span>
                                        </div>
                                        {{--<button class="toCart">В корзину</button>--}}

                                        <a href="javascript:void(0);" class="btn toCart addCartBtn not-icn" role="button" data-ng-click="cat.addToCart(product,product.qty,$event)"><em>В корзину</em><svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                            <div class="set itemProduct" ng-if="cat.currentCategory.id == 3">
                                <div class="setImg">
                                    <a data-fancybox="gallerySet" href="#" data-ng-href="@{{product.image_link}}" class="fancy">
                                        <picture>
                                            <source srcset="#" data-ng-srcset="@{{product.webp_link}}"
                                                    type="image/webp">
                                            <source srcset="#" data-ng-srcset="@{{product.image_link}}"
                                                    type="image/pjpeg">
                                            <img src="#" data-ng-src="@{{product.image_link}}"
                                                 alt="@{{product.seo_title ? product.seo_title : product.name}}">
                                        </picture>
                                    </a>
                                </div>
                                <div class="setDescription">
                                    <div class="setTitle">
                                        <p>@{{product.category.name}}</p>
                                        <h3 class="">&laquo;@{{product.name}}&raquo;</h3>
                                    </div>
                                    <div class="row text-center setOptions">
                                        <div class="col-4">
                                            <span class="setOpt">@{{product.size}}</span>
                                            <span>персон</span>
                                        </div>
                                        <div class="col-4">
                                            <span class="setOpt">@{{product.price}}</span>
                                            <span>тг</span>
                                        </div>
                                        <div class="col-4">
                                            <span class="setOpt">@{{product.weight}}</span>
                                            <span>грамм</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modalSet@{{ product.id }}" class="leftBtn">Подробнее</a>
                                <a href="javascript:void(0);" class="rightBtn addCartBtn" data-ng-click="cat.addToCart(product,product.qty,$event)"><em class="icn"></em>
                                    <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                        <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                                    </svg></a>
                            </div>
                            <div class="modal fade modal-product" id="modalSet@{{product.id}}" ng-if="cat.currentCategory.id == 3">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M1 1L16 16M16 1L1 16" stroke="#AEAEAE" stroke-width="2"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                        </button>
                                        <div class="product-content itemProduct">
                                            <picture>
                                                <source srcset="#" data-ng-srcset="@{{product.webp_link}}"
                                                        type="image/webp">
                                                <source srcset="#" data-ng-srcset="@{{product.image_link}}"
                                                        type="image/pjpeg">
                                                <img src="#" data-ng-src="@{{product.image_link}}" class="img-fluid"
                                                     alt="@{{product.seo_title ? product.seo_title : product.name}}">
                                            </picture>
                                            <div class="content">
                                                <div class="setTitle">
                                                    <p>@{{product.category.name}}</p>
                                                    <h3 class="">&laquo;@{{product.name}}&raquo;</h3>
                                                </div>
                                                <div class="description" ng-bind-html="product.description"></div>
                                                <div class="text-center setOptions">
                                                    <div class="col-7 options">
                                                        <div class="col-4">
                                                            <span class="setOpt">@{{product.size}}</span>
                                                            <span>персон</span>
                                                        </div>
                                                        <div class="col-4">
                                                            <span class="setOpt">@{{product.price}}</span>
                                                            <span>тг</span>
                                                        </div>
                                                        <div class="col-4">
                                                            <span class="setOpt">@{{product.weight}}</span>
                                                            <span>грамм</span>
                                                        </div>
                                                    </div>
                                                    <a href="javascript:void(0);" class="toCart add-to-cart addCartBtn not-icn"
                                                       data-ng-click="cat.addToCart(product,product.qty,$event)"><em>В корзину</em><svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                                            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                                                        </svg></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-3">
                            <ul data-ng-if="cat.filtered_products.length > cat.pageSize" data-uib-pagination
                                data-total-items="cat.filtered_products.length" data-ng-model="cat.currentPage"
                                data-max-size="4" data-template-url="/js/angular/templates/pagination.html"
                                class="pagination-sm" data-boundary-link="false" data-rotate="false"
                                data-items-per-page="cat.pageSize"></ul>
                            <div class="products" data-ng-if="!cat.products.length && !cat.loading">В данной
                                    категории еще нет товаров</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="/js/angular/catalog.controller.js"></script>
@endsection