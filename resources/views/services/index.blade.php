@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $page->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $page->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$page->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $page->meta_description))
@section('image',env('APP_URL').'/images/og.jpg')
@section('url',url()->current())
@section('page_class','services-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title])
    <h1 class="titleDark">{{$page->title}}</h1>
</div>
<section id="service">
    <div class="container">
        <div class="row pt-4">
            @foreach($services->chunk(2) as $s=>$chunk)
                <div class="row">
                    @if($s % 2 == 0)
                        @foreach($chunk as $k=>$service)
                            <div class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-7 col-sm-6 @else col-lg-5 col-sm-6 @endif @else col-12 @endif  serviceItem">
								<a href="{{route('pages.inner',[$page,$service])}}">
                                <div class="service row"
                                     style="background: url('{{Voyager::image($service->getThumbnail($service->image,'big'))}}') no-repeat left bottom;background-size: cover">
                                    <h3 class="col-12 align-self-center"><img
                                                src="{{Voyager::image($service->icon)}}"
                                                alt="{{$service->seo_title}}">{{$service->title}}
                                    </h3>
                                    <p class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-7 @else col-lg-10 @endif @else col-lg-7 @endif">{{$service->excerpt}}</p>
									<p class="col-12 d-lg-block"><span class="moree">Подробнее</span>
                                    </p>
                                </div>
								</a>
                            </div>
                        @endforeach
                    @else
                        @foreach($chunk as $k=>$service)
                            <div class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-5 col-sm-6 @else col-lg-7 col-sm-6 @endif @else col-12 @endif  serviceItem">
								<a href="{{route('pages.inner',[$page,$service])}}">
                                <div class="service row"
                                     style="background: url('{{Voyager::image($service->getThumbnail($service->image,'big'))}}') no-repeat left bottom;background-size: cover">
                                    <h3 class="col-12 align-self-center"><img
                                                src="{{Voyager::image($service->icon)}}"
                                                alt="{{$service->seo_title}}">{{$service->title}}
                                    </h3>
                                    <p class="@if($loop->last != $loop->first) @if($k % 2 == 0) col-lg-10 @else col-lg-7 @endif @else col-lg-7 @endif">{{$service->excerpt}}</p>
									<p class="col-12 d-lg-block"><span class="moree">Подробнее</span>
                                    </p>
                                </div>
								</a>
                            </div>
                        @endforeach
                    @endif
                </div>
            @endforeach
        </div>
        <div class="row pt-4">
            {!! $page->body !!}
        </div>
    </div>

    <div class="parallax" data-paroller-factor="0.3"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 200px; z-index:-1;height: 200px; left: 30px; top: 50%;background: url('img/stl.png') no-repeat center">
    </div>
    <div class="parallax" data-paroller-factor="-0.5"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 144px; height: 163px; left: 30px; bottom: 50%;background: url('img/sbl.png') no-repeat center">
    </div>
    <div class="parallax" data-paroller-factor="-0.5"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 257px; height: 265px; right: 30px; top: 0%;background: url('img/str.png') no-repeat center">
    </div>
    <div class="parallax" data-paroller-factor="0.3"
         data-paroller-type="foreground"
         data-paroller-direction="vertical"
         style="width: 294px; height: 309px; right: 30px; bottom: 0%;background: url('img/sbr.png') no-repeat center">
    </div>
</section>
@endsection