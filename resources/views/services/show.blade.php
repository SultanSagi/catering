@extends('app')
@section('title',(strlen($seoTitle) > 1 ? $seoTitle : $service->title))
@section('seo_title', (strlen($seoTitle) > 1 ? $seoTitle : $service->seo_title))
@section('meta_keywords',(strlen($keywords) > 1 ? $keywords :$service->meta_keywords))
@section('meta_description', (strlen($description) > 1 ? $description : $service->meta_description))
@section('image',$service->thumbic)
@section('url',url()->current())
@section('page_class','service-page')
@section('content')
<div class="breadcrumbs">
    @include('partials.breadcrumbs',['title'=>$page->title,'titleLink'=>route('pages.show',$page),'subtitle'=>$service->title])
    <h1 class="titleDark">{{$service->title}}</h1>
</div>
<section id="aboutUs">
    <div class="container">
        {!! $service->body !!}
    </div>
</section>
@endsection