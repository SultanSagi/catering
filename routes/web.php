<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::post('orders/{orderId}/update', 'Backend\ProductsController@orderUpdate')->name('orders.update');
    Route::post('orders/export', 'Backend\ProductsController@exportToExcel')->name('orders.export');
    Voyager::routes();
});
Route::get('/', 'PagesController@index')->name('home');
Route::get('/search', 'PagesController@search')->name('search');
Route::post('/reviews/add','ReviewsController@newReview')->name('reviews.add');

Route::get('/cart','CartController@index')->name('cart.index');

Route::post('/feedback','PagesController@feedback')->name('feedback');
Route::get('/thanks','PagesController@thanks')->name('thanks');

Route::get('/catalog/getCurrent/{slug}','PagesController@getCurrentCategory');
Route::get('/catalog/{categoryId}/products','PagesController@getAjaxProducts')->name('catalog.ajax.products');

Route::get('/cart','CartController@show')->name('cart.index');
Route::get('/cart/getCartContent','CartController@getCartContent');
Route::post('/cart/addToCart/{productId}','CartController@addToCart')->name('cart.add-to-cart');
Route::get('/cart/getCartItems','CartController@getCartItems');
Route::post('/cart/update','CartController@updateCart')->name('cart.update');
Route::post('/cart/remove','CartController@removeFromCart')->name('cart.remove');
Route::get('/cart/clear','CartController@destroyCart');
Route::get('/cart/remove/{itemId}','CartController@removeItem');
Route::get('/checkout','CartController@checkoutIndex')->name('cart.checkout');
Route::post('/checkout/submit','CartController@checkoutSubmit')->name('cart.checkout.submit');
Route::get('/checkout/{orderId}/thanks','CartController@checkoutThanks')->name('cart.checkout.thanks');
//Route::get('/checkout/{orderId}/cardPay','CartController@cardPay')->name('cart.checkout.cardPay');

//Route::get('/checkout/{orderId}/{paymentId}/awaiting','PaymentController@checkoutAwaiting')->name('cart.checkout.awaiting');
//Route::get('/checkout/{orderId}/{paymentId}/checkPaymentStatus','PaymentController@checkOrderPaymentStatus')->name('checkPaymentStatus');

Route::get('/{page}','PagesController@show')->name('pages.show');
Route::get('/{page}/{slug}','PagesController@showInner')->name('pages.inner');



